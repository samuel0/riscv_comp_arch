Library IEEE;
use IEEE.std_logic_1164.all;

entity flute is port (
    --Clock
    CLK100MHZ  : in std_logic;

    --Active low reset for CPU
    CPU_RESETN : in std_logic;

    LED        : out std_logic_vector(15 downto 0)

    );
end flute;

architecture flute_arch of flute is

    component mkSoC_Top
     port (
        CLK                           : in std_logic;
        RST_N                         : in std_logic;

        EN_to_raw_mem_request_get     : in std_logic;
        to_raw_mem_request_get        : out std_logic_vector(352 downto 0);
        RDY_to_raw_mem_request_get    : out std_logic;

        to_raw_mem_response_put       : in std_logic_vector(255 downto 0);
        EN_to_raw_mem_response_put    : in std_logic;
        RDY_to_raw_mem_response_put   : out std_logic;

        EN_get_to_console_get         : in std_logic;
        get_to_console_get            : out std_logic_vector(7 downto 0);
        RDY_get_to_console_get        : out std_logic;

        put_from_console_put          : in std_logic_vector(7 downto 0);
        EN_put_from_console_put       : in std_logic;
        RDY_put_from_console_put      : out std_logic;

        status                        : out std_logic_vector(7 downto 0);

        set_verbosity_verbosity       : in std_logic_vector(3 downto 0);
        set_verbosity_logdelay        : in std_logic_vector(63 downto 0);
        EN_set_verbosity              : in std_logic;
        RDY_set_verbosity             : out std_logic;

        set_watch_tohost_watch_tohost : in std_logic;
        set_watch_tohost_tohost_addr  : in std_logic_vector(63 downto 0);
        EN_set_watch_tohost           : in std_logic;
        RDY_set_watch_tohost          : out std_logic;

        mv_tohost_value               : out std_logic_vector(63 downto 0);
        RDY_mv_tohost_value           : out std_logic;

        EN_ma_ddr4_ready              : in std_logic;
        RDY_ma_ddr4_ready             : out std_logic;

        mv_status                     : out std_logic_vector(7 downto 0)
        );
    end component;


    --component mkCore
    --port(
    --    CLK                                                             : in std_logic;
    --    RST_N                                                           : in std_logic;
    
    --    -- action method cpu_reset_server_request_put
    --    cpu_reset_server_request_put                                    : in std_logic;
    --    EN_cpu_reset_server_request_put                                 : in std_logic;
    --    RDY_cpu_reset_server_request_put                                : out std_logic;
    
    --    -- actionvalue method cpu_reset_server_response_get
    --    EN_cpu_reset_server_response_get                                : in std_logic;
    --    cpu_reset_server_response_get                                   : out std_logic;
    --    RDY_cpu_reset_server_response_get                               : out std_logic;
    
    --    -- value method cpu_imem_master_m_awvalid
    --    cpu_imem_master_awvalid                                         : out std_logic;
    
    --    -- value method cpu_imem_master_m_awid
    --    cpu_imem_master_awid                                            : out std_logic_vector(3 downto 0);
    
    --    -- value method cpu_imem_master_m_awaddr
    --    cpu_imem_master_awaddr                                          : out std_logic_vector(63 downto 0);
    
    --    -- value method cpu_imem_master_m_awlen
    --    cpu_imem_master_awlen                                           : out std_logic_vector(7 downto 0);
    
    --    -- value method cpu_imem_master_m_awsize
    --    cpu_imem_master_awsize                                          : out std_logic_vector(2 downto 0);
    
    --    -- value method cpu_imem_master_m_awburst
    --    cpu_imem_master_awburst                                         : out std_logic_vector(1 downto 0);
    
    --    -- value method cpu_imem_master_m_awlock
    --    cpu_imem_master_awlock                                          : out std_logic;
    
    --    -- value method cpu_imem_master_m_awcache
    --    cpu_imem_master_awcache                                         : out std_logic_vector(3 downto 0);
    
    --    -- value method cpu_imem_master_m_awprot
    --    cpu_imem_master_awprot                                          : out std_logic_vector(2 downto 0);
    
    --    -- value method cpu_imem_master_m_awqos
    --    cpu_imem_master_awqos                                           : out std_logic_vector(3 downto 0);
    
    --    -- value method cpu_imem_master_m_awregion
    --    cpu_imem_master_awregion                                        : out std_logic_vector(3 downto 0);
    
    --    -- value method cpu_imem_master_m_awuser
    
    --    -- action method cpu_imem_master_m_awready
    --    cpu_imem_master_awready                                         : in std_logic;
    
    --    -- value method cpu_imem_master_m_wvalid
    --    cpu_imem_master_wvalid                                          : out std_logic;
    
    --    -- value method cpu_imem_master_m_wdata
    --    cpu_imem_master_wdata                                           : out std_logic_vector(63 downto 0);
    
    --    -- value method cpu_imem_master_m_wstrb
    --    cpu_imem_master_wstrb                                           : out std_logic_vector(7 downto 0);
    
    --    -- value method cpu_imem_master_m_wlast
    --    cpu_imem_master_wlast                                           : out std_logic;
    
    --    -- value method cpu_imem_master_m_wuser
    
    --    -- action method cpu_imem_master_m_wready
    --    cpu_imem_master_wready                                          : in std_logic;
    
    --    -- action method cpu_imem_master_m_bvalid
    --    cpu_imem_master_bvalid                                          : in std_logic;
    --    cpu_imem_master_bid                                             : in std_logic_vector(3 downto 0);
    --    cpu_imem_master_bresp                                           : in std_logic_vector(1 downto 0);
    
    --    -- value method cpu_imem_master_m_bready
    --    cpu_imem_master_bready                                          : out std_logic;
    
    --    -- value method cpu_imem_master_m_arvalid
    --    cpu_imem_master_arvalid                                         : out std_logic;
    
    --    -- value method cpu_imem_master_m_arid
    --    cpu_imem_master_arid                                            : out std_logic_vector(3 downto 0);
    
    --    -- value method cpu_imem_master_m_araddr
    --    cpu_imem_master_araddr                                          : out std_logic_vector(63 downto 0);
    
    --    -- value method cpu_imem_master_m_arlen
    --    cpu_imem_master_arlen                                           : out std_logic_vector(7 downto 0);
    
    --    -- value method cpu_imem_master_m_arsize
    --    cpu_imem_master_arsize                                          : out std_logic_vector(2 downto 0);
    
    --    -- value method cpu_imem_master_m_arburst
    --    cpu_imem_master_arburst                                         : out std_logic_vector(1 downto 0);
    
    --    -- value method cpu_imem_master_m_arlock
    --    cpu_imem_master_arlock                                          : out std_logic;
    
    --    -- value method cpu_imem_master_m_arcache
    --    cpu_imem_master_arcache                                         : out std_logic_vector(3 downto 0);
    
    --    -- value method cpu_imem_master_m_arprot
    --    cpu_imem_master_arprot                                          : out std_logic_vector(2 downto 0);
    
    --    -- value method cpu_imem_master_m_arqos
    --    cpu_imem_master_arqos                                           : out std_logic_vector(3 downto 0);
    
    --    -- value method cpu_imem_master_m_arregion
    --    cpu_imem_master_arregion                                        : out std_logic_vector(3 downto 0);
    
    --    -- value method cpu_imem_master_m_aruser
    
    --    -- action method cpu_imem_master_m_arready
    --    cpu_imem_master_arready                                         : in std_logic;
    
    --    -- action method cpu_imem_master_m_rvalid
    --    cpu_imem_master_rvalid                                          : in std_logic;
    --    cpu_imem_master_rid                                             : in std_logic_vector(3 downto 0);
    --    cpu_imem_master_rdata                                           : in std_logic_vector(63 downto 0);
    --    cpu_imem_master_rresp                                           : in std_logic_vector(1 downto 0);
    --    cpu_imem_master_rlast                                           : in std_logic;
    
    --    -- value method cpu_imem_master_m_rready
    --    cpu_imem_master_rready                                          : out std_logic;
    
    --    -- value method core_mem_master_m_awvalid
    --    core_mem_master_awvalid                                         : out std_logic;
    
    --    -- value method core_mem_master_m_awid
    --    core_mem_master_awid                                            : out std_logic_vector(3 downto 0);
    
    --    -- value method core_mem_master_m_awaddr
    --    core_mem_master_awaddr                                          : out std_logic_vector(63 downto 0);
    
    --    -- value method core_mem_master_m_awlen
    --    core_mem_master_awlen                                           : out std_logic_vector(7 downto 0);
    
    --    -- value method core_mem_master_m_awsize
    --    core_mem_master_awsize                                          : out std_logic_vector(2 downto 0);
    
    --    -- value method core_mem_master_m_awburst
    --    core_mem_master_awburst                                         : out std_logic_vector(1 downto 0);
    
    --    -- value method core_mem_master_m_awlock
    --    core_mem_master_awlock                                          : out std_logic;
    
    --    -- value method core_mem_master_m_awcache
    --    core_mem_master_awcache                                         : out std_logic_vector(3 downto 0);
    
    --    -- value method core_mem_master_m_awprot
    --    core_mem_master_awprot                                          : out std_logic_vector(2 downto 0);
    
    --    -- value method core_mem_master_m_awqos
    --    core_mem_master_awqos                                           : out std_logic_vector(3 downto 0);
    
    --    -- value method core_mem_master_m_awregion
    --    core_mem_master_awregion                                        : out std_logic_vector(3 downto 0);
    
    --    -- value method core_mem_master_m_awuser
    
    --    -- action method core_mem_master_m_awready
    --    core_mem_master_awready                                         : in std_logic;
    
    --    -- value method core_mem_master_m_wvalid
    --    core_mem_master_wvalid                                          : out std_logic;
    
    --    -- value method core_mem_master_m_wdata
    --    core_mem_master_wdata                                           : out std_logic_vector(63 downto 0);
    
    --    -- value method core_mem_master_m_wstrb
    --    core_mem_master_wstrb                                           : out std_logic_vector(7 downto 0);
    
    --    -- value method core_mem_master_m_wlast
    --    core_mem_master_wlast                                           : out std_logic;
    
    --    -- value method core_mem_master_m_wuser
    
    --    -- action method core_mem_master_m_wready
    --    core_mem_master_wready                                          : in std_logic;
    
    --    -- action method core_mem_master_m_bvalid
    --    core_mem_master_bvalid                                          : in std_logic;
    --    core_mem_master_bid                                             : in std_logic_vector(3 downto 0);
    --    core_mem_master_bresp                                           : in std_logic_vector(1 downto 0);
    
    --    -- value method core_mem_master_m_bready
    --    core_mem_master_bready                                          : out std_logic;
    
    --    -- value method core_mem_master_m_arvalid
    --    core_mem_master_arvalid                                         : out std_logic;
    
    --    -- value method core_mem_master_m_arid
    --    core_mem_master_arid                                            : out std_logic_vector(3 downto 0);
    
    --    -- value method core_mem_master_m_araddr
    --    core_mem_master_araddr                                          : out std_logic_vector(63 downto 0);
    
    --    -- value method core_mem_master_m_arlen
    --    core_mem_master_arlen                                           : out std_logic_vector(7 downto 0);
    
    --    -- value method core_mem_master_m_arsize
    --    core_mem_master_arsize                                          : out std_logic_vector(2 downto 0);
    
    --    -- value method core_mem_master_m_arburst
    --    core_mem_master_arburst                                         : out std_logic_vector(1 downto 0);
    
    --    -- value method core_mem_master_m_arlock
    --    core_mem_master_arlock                                          : out std_logic;
    
    --    -- value method core_mem_master_m_arcache
    --    core_mem_master_arcache                                         : out std_logic_vector(3 downto 0);
    
    --    -- value method core_mem_master_m_arprot
    --    core_mem_master_arprot                                          : out std_logic_vector(2 downto 0);
    
    --    -- value method core_mem_master_m_arqos
    --    core_mem_master_arqos                                           : out std_logic_vector(3 downto 0);
    
    --    -- value method core_mem_master_m_arregion
    --    core_mem_master_arregion                                        : out std_logic_vector(3 downto 0);
    
    --    -- value method core_mem_master_m_aruser
    
    --    -- action method core_mem_master_m_arready
    --    core_mem_master_arready                                         : in std_logic;
    
    --    -- action method core_mem_master_m_rvalid
    --    core_mem_master_rvalid                                          : in std_logic;
    --    core_mem_master_rid                                             : in std_logic_vector(3 downto 0);
    --    core_mem_master_rdata                                           : in std_logic_vector(63 downto 0);
    --    core_mem_master_rresp                                           : in std_logic_vector(1 downto 0);
    --    core_mem_master_rlast                                           : in std_logic;
    
    --    -- value method core_mem_master_m_rready
    --    core_mem_master_rready                                          : out std_logic;
    
    --    -- action method dma_server_m_awvalid
    --    dma_server_awvalid                                              : in std_logic;
    --    dma_server_awid                                                 : in std_logic_vector(5 downto 0);
    --    dma_server_awaddr                                               : in std_logic_vector(63 downto 0);
    --    dma_server_awlen                                                : in std_logic_vector(7 downto 0);
    --    dma_server_awsize                                               : in std_logic_vector(2 downto 0);
    --    dma_server_awburst                                              : in std_logic_vector(1 downto 0);
    --    dma_server_awlock                                               : in std_logic;
    --    dma_server_awcache                                              : in std_logic_vector(3 downto 0);
    --    dma_server_awprot                                               : in std_logic_vector(2 downto 0);
    --    dma_server_awqos                                                : in std_logic_vector(3 downto 0);
    --    dma_server_awregion                                             : in std_logic_vector(3 downto 0);
    
    --    -- value method dma_server_m_awready
    --    dma_server_awready                                              : out std_logic;
    
    --    -- action method dma_server_m_wvalid
    --    dma_server_wvalid                                               : in std_logic;
    --    dma_server_wdata                                                : in std_logic_vector(511 downto 0);
    --    dma_server_wstrb                                                : in std_logic_vector(63 downto 0);
    --    dma_server_wlast                                                : in std_logic;
    
    --    -- value method dma_server_m_wready
    --    dma_server_wready                                               : out std_logic;
    
    --    -- value method dma_server_m_bvalid
    --    dma_server_bvalid                                               : out std_logic;
    
    --    -- value method dma_server_m_bid
    --    dma_server_bid                                                  : out std_logic_vector(5 downto 0);
    
    --    -- value method dma_server_m_bresp
    --    dma_server_bresp                                                : out std_logic_vector(1 downto 0);
    
    --    -- value method dma_server_m_buser
    
    --    -- action method dma_server_m_bready
    --    dma_server_bready                                               : in std_logic;
    
    --    -- action method dma_server_m_arvalid
    --    dma_server_arvalid                                              : in std_logic;
    --    dma_server_arid                                                 : in std_logic_vector(5 downto 0);
    --    dma_server_araddr                                               : in std_logic_vector(63 downto 0);
    --    dma_server_arlen                                                : in std_logic_vector(7 downto 0);
    --    dma_server_arsize                                               : in std_logic_vector(2 downto 0);
    --    dma_server_arburst                                              : in std_logic_vector(1 downto 0);
    --    dma_server_arlock                                               : in std_logic;
    --    dma_server_arcache                                              : in std_logic_vector(3 downto 0);
    --    dma_server_arprot                                               : in std_logic_vector(2 downto 0);
    --    dma_server_arqos                                                : in std_logic_vector(3 downto 0);
    --    dma_server_arregion                                             : in std_logic_vector(3 downto 0);
    
    --    -- value method dma_server_m_arready
    --    dma_server_arready                                              : out std_logic;
    
    --    -- value method dma_server_m_rvalid
    --    dma_server_rvalid                                               : out std_logic;
    
    --    -- value method dma_server_m_rid
    --    dma_server_rid                                                  : out std_logic_vector(5 downto 0);
    
    --    -- value method dma_server_m_rdata
    --    dma_server_rdata                                                : out std_logic_vector(511 downto 0);
    
    --    -- value method dma_server_m_rresp
    --    dma_server_rresp                                                : out std_logic_vector(1 downto 0);
    
    --    -- value method dma_server_m_rlast
    --    dma_server_rlast                                                : out std_logic;
    
    --    -- value method dma_server_m_ruser
    
    --    -- action method dma_server_m_rready
    --    dma_server_rready                                               : in std_logic;
    
    --    -- action method core_external_interrupt_sources_0_m_interrupt_req
    --    core_external_interrupt_sources_0_m_interrupt_req_set_not_clear : in std_logic;
    
    --    -- action method core_external_interrupt_sources_1_m_interrupt_req
    --    core_external_interrupt_sources_1_m_interrupt_req_set_not_clear : in std_logic;
    
    --    -- action method core_external_interrupt_sources_2_m_interrupt_req
    --    core_external_interrupt_sources_2_m_interrupt_req_set_not_clear : in std_logic;
    
    --    -- action method core_external_interrupt_sources_3_m_interrupt_req
    --    core_external_interrupt_sources_3_m_interrupt_req_set_not_clear : in std_logic;
    
    --    -- action method core_external_interrupt_sources_4_m_interrupt_req
    --    core_external_interrupt_sources_4_m_interrupt_req_set_not_clear : in std_logic;
    
    --    -- action method core_external_interrupt_sources_5_m_interrupt_req
    --    core_external_interrupt_sources_5_m_interrupt_req_set_not_clear : in std_logic;
    
    --    -- action method core_external_interrupt_sources_6_m_interrupt_req
    --    core_external_interrupt_sources_6_m_interrupt_req_set_not_clear : in std_logic;
    
    --    -- action method core_external_interrupt_sources_7_m_interrupt_req
    --    core_external_interrupt_sources_7_m_interrupt_req_set_not_clear : in std_logic;
    
    --    -- action method core_external_interrupt_sources_8_m_interrupt_req
    --    core_external_interrupt_sources_8_m_interrupt_req_set_not_clear : in std_logic;
    
    --    -- action method core_external_interrupt_sources_9_m_interrupt_req
    --    core_external_interrupt_sources_9_m_interrupt_req_set_not_clear : in std_logic;
    
    --    -- action method core_external_interrupt_sources_10_m_interrupt_req
    --    core_external_interrupt_sources_10_m_interrupt_req_set_not_clear: in std_logic;
    
    --    -- action method core_external_interrupt_sources_11_m_interrupt_req
    --    core_external_interrupt_sources_11_m_interrupt_req_set_not_clear: in std_logic;
    
    --    -- action method core_external_interrupt_sources_12_m_interrupt_req
    --    core_external_interrupt_sources_12_m_interrupt_req_set_not_clear: in std_logic;
    
    --    -- action method core_external_interrupt_sources_13_m_interrupt_req
    --    core_external_interrupt_sources_13_m_interrupt_req_set_not_clear: in std_logic;
    
    --    -- action method core_external_interrupt_sources_14_m_interrupt_req
    --    core_external_interrupt_sources_14_m_interrupt_req_set_not_clear: in std_logic;
    
    --    -- action method core_external_interrupt_sources_15_m_interrupt_req
    --    core_external_interrupt_sources_15_m_interrupt_req_set_not_clear: in std_logic;
    
    --    -- action method nmi_req
    --    nmi_req_set_not_clear                                           : in std_logic;
    
    --    -- action method set_verbosity
    --    set_verbosity_verbosity                                         : in std_logic_vector(3 downto 0);
    --    set_verbosity_logdelay                                          : in std_logic_vector(63 downto 0);
    --    EN_set_verbosity                                                : in std_logic;
    --    RDY_set_verbosity                                               : out std_logic;
    
    --    -- action method set_watch_tohost
    --    set_watch_tohost_watch_tohost                                   : in std_logic;
    --    set_watch_tohost_tohost_addr                                    : in std_logic_vector(63 downto 0);
    --    EN_set_watch_tohost                                             : in std_logic;
    --    RDY_set_watch_tohost                                            : out std_logic;
    
    --    -- value method mv_tohost_value
    --    mv_tohost_value                                                 : out std_logic_vector(63 downto 0);
    --    RDY_mv_tohost_value                                             : out std_logic;
    
    --    -- action method ma_ddr4_ready
    --    EN_ma_ddr4_ready                                                : in std_logic;
    --    RDY_ma_ddr4_ready                                               : out std_logic;
    
    --    -- value method mv_status
    --    mv_status                                                       : out std_logic_vector(7 downto 0)
    --);
    --end component;


    --component mkFabric_AXI4
    --port(
    --    CLK                      : in std_logic;
    --    RST_N                    : in std_logic;
    
    --    -- action method reset
    --    EN_reset                 : in std_logic;
    --    RDY_reset                : out std_logic;
    
    --    -- action method set_verbosity
    --    set_verbosity_verbosity  : in std_logic_vector(3 downto 0);
    --    EN_set_verbosity         : in std_logic;
    --    RDY_set_verbosity        : out std_logic;
    
    --    -- action method v_from_masters_0_m_awvalid
    --    v_from_masters_0_awvalid : in std_logic;
    --    v_from_masters_0_awid    : in std_logic_vector(3 downto 0);
    --    v_from_masters_0_awaddr  : in std_logic_vector(63 downto 0);
    --    v_from_masters_0_awlen   : in std_logic_vector(7 downto 0);
    --    v_from_masters_0_awsize  : in std_logic_vector(2 downto 0);
    --    v_from_masters_0_awburst : in std_logic_vector(1 downto 0);
    --    v_from_masters_0_awlock  : in std_logic;
    --    v_from_masters_0_awcache : in std_logic_vector(3 downto 0);
    --    v_from_masters_0_awprot  : in std_logic_vector(2 downto 0);
    --    v_from_masters_0_awqos   : in std_logic_vector(3 downto 0);
    --    v_from_masters_0_awregion: in std_logic_vector(3 downto 0);
    
    --    -- value method v_from_masters_0_m_awready
    --    v_from_masters_0_awready : out std_logic;
    
    --    -- action method v_from_masters_0_m_wvalid
    --    v_from_masters_0_wvalid  : in std_logic;
    --    v_from_masters_0_wdata   : in std_logic_vector(63 downto 0);
    --    v_from_masters_0_wstrb   : in std_logic_vector(7 downto 0);
    --    v_from_masters_0_wlast   : in std_logic;
    
    --    -- value method v_from_masters_0_m_wready
    --    v_from_masters_0_wready  : out std_logic;
    
    --    -- value method v_from_masters_0_m_bvalid
    --    v_from_masters_0_bvalid  : out std_logic;
    
    --    -- value method v_from_masters_0_m_bid
    --    v_from_masters_0_bid     : out std_logic_vector(3 downto 0);
    
    --    -- value method v_from_masters_0_m_bresp
    --    v_from_masters_0_bresp   : out std_logic_vector(1 downto 0);
    
    --    -- value method v_from_masters_0_m_buser
    
    --    -- action method v_from_masters_0_m_bready
    --    v_from_masters_0_bready  : in std_logic;
    
    --    -- action method v_from_masters_0_m_arvalid
    --    v_from_masters_0_arvalid : in std_logic;
    --    v_from_masters_0_arid    : in std_logic_vector(3 downto 0);
    --    v_from_masters_0_araddr  : in std_logic_vector(63 downto 0);
    --    v_from_masters_0_arlen   : in std_logic_vector(7 downto 0);
    --    v_from_masters_0_arsize  : in std_logic_vector(2 downto 0);
    --    v_from_masters_0_arburst : in std_logic_vector(1 downto 0);
    --    v_from_masters_0_arlock  : in std_logic;
    --    v_from_masters_0_arcache : in std_logic_vector(3 downto 0);
    --    v_from_masters_0_arprot  : in std_logic_vector(2 downto 0);
    --    v_from_masters_0_arqos   : in std_logic_vector(3 downto 0);
    --    v_from_masters_0_arregion: in std_logic_vector(3 downto 0);
    
    --    -- value method v_from_masters_0_m_arready
    --    v_from_masters_0_arready : out std_logic;
    
    --    -- value method v_from_masters_0_m_rvalid
    --    v_from_masters_0_rvalid  : out std_logic;
    
    --    -- value method v_from_masters_0_m_rid
    --    v_from_masters_0_rid     : out std_logic_vector(3 downto 0);
    
    --    -- value method v_from_masters_0_m_rdata
    --    v_from_masters_0_rdata   : out std_logic_vector(63 downto 0);
    
    --    -- value method v_from_masters_0_m_rresp
    --    v_from_masters_0_rresp   : out std_logic_vector(1 downto 0);
    
    --    -- value method v_from_masters_0_m_rlast
    --    v_from_masters_0_rlast   : out std_logic;
    
    --    -- value method v_from_masters_0_m_ruser
    
    --    -- action method v_from_masters_0_m_rready
    --    v_from_masters_0_rready  : in std_logic;
    
    --    -- action method v_from_masters_1_m_awvalid
    --    v_from_masters_1_awvalid : in std_logic;
    --    v_from_masters_1_awid    : in std_logic_vector(3 downto 0);
    --    v_from_masters_1_awaddr  : in std_logic_vector(63 downto 0);
    --    v_from_masters_1_awlen   : in std_logic_vector(7 downto 0);
    --    v_from_masters_1_awsize  : in std_logic_vector(2 downto 0);
    --    v_from_masters_1_awburst : in std_logic_vector(1 downto 0);
    --    v_from_masters_1_awlock  : in std_logic;
    --    v_from_masters_1_awcache : in std_logic_vector(3 downto 0);
    --    v_from_masters_1_awprot  : in std_logic_vector(2 downto 0);
    --    v_from_masters_1_awqos   : in std_logic_vector(3 downto 0);
    --    v_from_masters_1_awregion: in std_logic_vector(3 downto 0);
    
    --    -- value method v_from_masters_1_m_awready
    --    v_from_masters_1_awready : out std_logic;
    
    --    -- action method v_from_masters_1_m_wvalid
    --    v_from_masters_1_wvalid  : in std_logic;
    --    v_from_masters_1_wdata   : in std_logic_vector(63 downto 0);
    --    v_from_masters_1_wstrb   : in std_logic_vector(7 downto 0);
    --    v_from_masters_1_wlast   : in std_logic;
    
    --    -- value method v_from_masters_1_m_wready
    --    v_from_masters_1_wready  : out std_logic;
    
    --    -- value method v_from_masters_1_m_bvalid
    --    v_from_masters_1_bvalid  : out std_logic;
    
    --    -- value method v_from_masters_1_m_bid
    --    v_from_masters_1_bid     : out std_logic_vector(3 downto 0);
    
    --    -- value method v_from_masters_1_m_bresp
    --    v_from_masters_1_bresp   : out std_logic_vector(1 downto 0);
    
    --    -- value method v_from_masters_1_m_buser
    
    --    -- action method v_from_masters_1_m_bready
    --    v_from_masters_1_bready  : in std_logic;
    
    --    -- action method v_from_masters_1_m_arvalid
    --    v_from_masters_1_arvalid : in std_logic;
    --    v_from_masters_1_arid    : in std_logic_vector(3 downto 0);
    --    v_from_masters_1_araddr  : in std_logic_vector(63 downto 0);
    --    v_from_masters_1_arlen   : in std_logic_vector(7 downto 0);
    --    v_from_masters_1_arsize  : in std_logic_vector(2 downto 0);
    --    v_from_masters_1_arburst : in std_logic_vector(1 downto 0);
    --    v_from_masters_1_arlock  : in std_logic;
    --    v_from_masters_1_arcache : in std_logic_vector(3 downto 0);
    --    v_from_masters_1_arprot  : in std_logic_vector(2 downto 0);
    --    v_from_masters_1_arqos   : in std_logic_vector(3 downto 0);
    --    v_from_masters_1_arregion: in std_logic_vector(3 downto 0);
    
    --    -- value method v_from_masters_1_m_arready
    --    v_from_masters_1_arready : out std_logic;
    
    --    -- value method v_from_masters_1_m_rvalid
    --    v_from_masters_1_rvalid  : out std_logic;
    
    --    -- value method v_from_masters_1_m_rid
    --    v_from_masters_1_rid     : out std_logic_vector(3 downto 0);
    
    --    -- value method v_from_masters_1_m_rdata
    --    v_from_masters_1_rdata   : out std_logic_vector(63 downto 0);
    
    --    -- value method v_from_masters_1_m_rresp
    --    v_from_masters_1_rresp   : out std_logic_vector(1 downto 0);
    
    --    -- value method v_from_masters_1_m_rlast
    --    v_from_masters_1_rlast   : out std_logic;
    
    --    -- value method v_from_masters_1_m_ruser
    
    --    -- action method v_from_masters_1_m_rready
    --    v_from_masters_1_rready  : in std_logic;
    
    --    -- value method v_to_slaves_0_m_awvalid
    --    v_to_slaves_0_awvalid    : out std_logic;
    
    --    -- value method v_to_slaves_0_m_awid
    --    v_to_slaves_0_awid       : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_0_m_awaddr
    --    v_to_slaves_0_awaddr     : out std_logic_vector(63 downto 0);
    
    --    -- value method v_to_slaves_0_m_awlen
    --    v_to_slaves_0_awlen      : out std_logic_vector(7 downto 0);
    
    --    -- value method v_to_slaves_0_m_awsize
    --    v_to_slaves_0_awsize     : out std_logic_vector(2 downto 0);
    
    --    -- value method v_to_slaves_0_m_awburst
    --    v_to_slaves_0_awburst    : out std_logic_vector(1 downto 0);
    
    --    -- value method v_to_slaves_0_m_awlock
    --    v_to_slaves_0_awlock     : out std_logic;
    
    --    -- value method v_to_slaves_0_m_awcache
    --    v_to_slaves_0_awcache    : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_0_m_awprot
    --    v_to_slaves_0_awprot     : out std_logic_vector(2 downto 0);
    
    --    -- value method v_to_slaves_0_m_awqos
    --    v_to_slaves_0_awqos      : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_0_m_awregion
    --    v_to_slaves_0_awregion   : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_0_m_awuser
    
    --    -- action method v_to_slaves_0_m_awready
    --    v_to_slaves_0_awready    : in std_logic;
    
    --    -- value method v_to_slaves_0_m_wvalid
    --    v_to_slaves_0_wvalid     : out std_logic;
    
    --    -- value method v_to_slaves_0_m_wdata
    --    v_to_slaves_0_wdata      : out std_logic_vector(63 downto 0);
    
    --    -- value method v_to_slaves_0_m_wstrb
    --    v_to_slaves_0_wstrb      : out std_logic_vector(7 downto 0);
    
    --    -- value method v_to_slaves_0_m_wlast
    --    v_to_slaves_0_wlast      : out std_logic;
    
    --    -- value method v_to_slaves_0_m_wuser
    
    --    -- action method v_to_slaves_0_m_wready
    --    v_to_slaves_0_wready     : in std_logic;
    
    --    -- action method v_to_slaves_0_m_bvalid
    --    v_to_slaves_0_bvalid     : in std_logic;
    --    v_to_slaves_0_bid        : in std_logic_vector(3 downto 0);
    --    v_to_slaves_0_bresp      : in std_logic_vector(1 downto 0);
    
    --    -- value method v_to_slaves_0_m_bready
    --    v_to_slaves_0_bready     : out std_logic;
    
    --    -- value method v_to_slaves_0_m_arvalid
    --    v_to_slaves_0_arvalid    : out std_logic;
    
    --    -- value method v_to_slaves_0_m_arid
    --    v_to_slaves_0_arid       : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_0_m_araddr
    --    v_to_slaves_0_araddr     : out std_logic_vector(63 downto 0);
    
    --    -- value method v_to_slaves_0_m_arlen
    --    v_to_slaves_0_arlen      : out std_logic_vector(7 downto 0);
    
    --    -- value method v_to_slaves_0_m_arsize
    --    v_to_slaves_0_arsize     : out std_logic_vector(2 downto 0);
    
    --    -- value method v_to_slaves_0_m_arburst
    --    v_to_slaves_0_arburst    : out std_logic_vector(1 downto 0);
    
    --    -- value method v_to_slaves_0_m_arlock
    --    v_to_slaves_0_arlock     : out std_logic;
    
    --    -- value method v_to_slaves_0_m_arcache
    --    v_to_slaves_0_arcache    : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_0_m_arprot
    --    v_to_slaves_0_arprot     : out std_logic_vector(2 downto 0);
    
    --    -- value method v_to_slaves_0_m_arqos
    --    v_to_slaves_0_arqos      : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_0_m_arregion
    --    v_to_slaves_0_arregion   : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_0_m_aruser
    
    --    -- action method v_to_slaves_0_m_arready
    --    v_to_slaves_0_arready    : in std_logic;
    
    --    -- action method v_to_slaves_0_m_rvalid
    --    v_to_slaves_0_rvalid     : in std_logic;
    --    v_to_slaves_0_rid        : in std_logic_vector(3 downto 0);
    --    v_to_slaves_0_rdata      : in std_logic_vector(63 downto 0);
    --    v_to_slaves_0_rresp      : in std_logic_vector(1 downto 0);
    --    v_to_slaves_0_rlast      : in std_logic;
    
    --    -- value method v_to_slaves_0_m_rready
    --    v_to_slaves_0_rready     : out std_logic;
    
    --    -- value method v_to_slaves_1_m_awvalid
    --    v_to_slaves_1_awvalid    : out std_logic;
    
    --    -- value method v_to_slaves_1_m_awid
    --    v_to_slaves_1_awid       : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_1_m_awaddr
    --    v_to_slaves_1_awaddr     : out std_logic_vector(63 downto 0);
    
    --    -- value method v_to_slaves_1_m_awlen
    --    v_to_slaves_1_awlen      : out std_logic_vector(7 downto 0);
    
    --    -- value method v_to_slaves_1_m_awsize
    --    v_to_slaves_1_awsize     : out std_logic_vector(2 downto 0);
    
    --    -- value method v_to_slaves_1_m_awburst
    --    v_to_slaves_1_awburst    : out std_logic_vector(1 downto 0);
    
    --    -- value method v_to_slaves_1_m_awlock
    --    v_to_slaves_1_awlock     : out std_logic;
    
    --    -- value method v_to_slaves_1_m_awcache
    --    v_to_slaves_1_awcache    : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_1_m_awprot
    --    v_to_slaves_1_awprot     : out std_logic_vector(2 downto 0);
    
    --    -- value method v_to_slaves_1_m_awqos
    --    v_to_slaves_1_awqos      : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_1_m_awregion
    --    v_to_slaves_1_awregion   : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_1_m_awuser
    
    --    -- action method v_to_slaves_1_m_awready
    --    v_to_slaves_1_awready    : in std_logic;
    
    --    -- value method v_to_slaves_1_m_wvalid
    --    v_to_slaves_1_wvalid     : out std_logic;
    
    --    -- value method v_to_slaves_1_m_wdata
    --    v_to_slaves_1_wdata      : out std_logic_vector(63 downto 0);
    
    --    -- value method v_to_slaves_1_m_wstrb
    --    v_to_slaves_1_wstrb      : out std_logic_vector(7 downto 0);
    
    --    -- value method v_to_slaves_1_m_wlast
    --    v_to_slaves_1_wlast      : out std_logic;
    
    --    -- value method v_to_slaves_1_m_wuser
    
    --    -- action method v_to_slaves_1_m_wready
    --    v_to_slaves_1_wready     : in std_logic;
    
    --    -- action method v_to_slaves_1_m_bvalid
    --    v_to_slaves_1_bvalid     : in std_logic;
    --    v_to_slaves_1_bid        : in std_logic_vector(3 downto 0);
    --    v_to_slaves_1_bresp      : in std_logic_vector(1 downto 0);
    
    --    -- value method v_to_slaves_1_m_bready
    --    v_to_slaves_1_bready     : out std_logic;
    
    --    -- value method v_to_slaves_1_m_arvalid
    --    v_to_slaves_1_arvalid    : out std_logic;
    
    --    -- value method v_to_slaves_1_m_arid
    --    v_to_slaves_1_arid       : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_1_m_araddr
    --    v_to_slaves_1_araddr     : out std_logic_vector(63 downto 0);
    
    --    -- value method v_to_slaves_1_m_arlen
    --    v_to_slaves_1_arlen      : out std_logic_vector(7 downto 0);
    
    --    -- value method v_to_slaves_1_m_arsize
    --    v_to_slaves_1_arsize     : out std_logic_vector(2 downto 0);
    
    --    -- value method v_to_slaves_1_m_arburst
    --    v_to_slaves_1_arburst    : out std_logic_vector(1 downto 0);
    
    --    -- value method v_to_slaves_1_m_arlock
    --    v_to_slaves_1_arlock     : out std_logic;
    
    --    -- value method v_to_slaves_1_m_arcache
    --    v_to_slaves_1_arcache    : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_1_m_arprot
    --    v_to_slaves_1_arprot     : out std_logic_vector(2 downto 0);
    
    --    -- value method v_to_slaves_1_m_arqos
    --    v_to_slaves_1_arqos      : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_1_m_arregion
    --    v_to_slaves_1_arregion   : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_1_m_aruser
    
    --    -- action method v_to_slaves_1_m_arready
    --    v_to_slaves_1_arready    : in std_logic;
    
    --    -- action method v_to_slaves_1_m_rvalid
    --    v_to_slaves_1_rvalid     : in std_logic;
    --    v_to_slaves_1_rid        : in std_logic_vector(3 downto 0);
    --    v_to_slaves_1_rdata      : in std_logic_vector(63 downto 0);
    --    v_to_slaves_1_rresp      : in std_logic_vector(1 downto 0);
    --    v_to_slaves_1_rlast      : in std_logic;
    
    --    -- value method v_to_slaves_1_m_rready
    --    v_to_slaves_1_rready     : out std_logic;
    
    --    -- value method v_to_slaves_2_m_awvalid
    --    v_to_slaves_2_awvalid    : out std_logic;
    
    --    -- value method v_to_slaves_2_m_awid
    --    v_to_slaves_2_awid       : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_2_m_awaddr
    --    v_to_slaves_2_awaddr     : out std_logic_vector(63 downto 0);
    
    --    -- value method v_to_slaves_2_m_awlen
    --    v_to_slaves_2_awlen      : out std_logic_vector(7 downto 0);
    
    --    -- value method v_to_slaves_2_m_awsize
    --    v_to_slaves_2_awsize     : out std_logic_vector(2 downto 0);
    
    --    -- value method v_to_slaves_2_m_awburst
    --    v_to_slaves_2_awburst    : out std_logic_vector(1 downto 0);
    
    --    -- value method v_to_slaves_2_m_awlock
    --    v_to_slaves_2_awlock     : out std_logic;
    
    --    -- value method v_to_slaves_2_m_awcache
    --    v_to_slaves_2_awcache    : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_2_m_awprot
    --    v_to_slaves_2_awprot     : out std_logic_vector(2 downto 0);
    
    --    -- value method v_to_slaves_2_m_awqos
    --    v_to_slaves_2_awqos      : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_2_m_awregion
    --    v_to_slaves_2_awregion   : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_2_m_awuser
    
    --    -- action method v_to_slaves_2_m_awready
    --    v_to_slaves_2_awready    : in std_logic;
    
    --    -- value method v_to_slaves_2_m_wvalid
    --    v_to_slaves_2_wvalid     : out std_logic;
    
    --    -- value method v_to_slaves_2_m_wdata
    --    v_to_slaves_2_wdata      : out std_logic_vector(63 downto 0);
    
    --    -- value method v_to_slaves_2_m_wstrb
    --    v_to_slaves_2_wstrb      : out std_logic_vector(7 downto 0);
    
    --    -- value method v_to_slaves_2_m_wlast
    --    v_to_slaves_2_wlast      : out std_logic;
    
    --    -- value method v_to_slaves_2_m_wuser
    
    --    -- action method v_to_slaves_2_m_wready
    --    v_to_slaves_2_wready     : in std_logic;
    
    --    -- action method v_to_slaves_2_m_bvalid
    --    v_to_slaves_2_bvalid     : in std_logic;
    --    v_to_slaves_2_bid        : in std_logic_vector(3 downto 0);
    --    v_to_slaves_2_bresp      : in std_logic_vector(1 downto 0);
    
    --    -- value method v_to_slaves_2_m_bready
    --    v_to_slaves_2_bready     : out std_logic;
    
    --    -- value method v_to_slaves_2_m_arvalid
    --    v_to_slaves_2_arvalid    : out std_logic;
    
    --    -- value method v_to_slaves_2_m_arid
    --    v_to_slaves_2_arid       : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_2_m_araddr
    --    v_to_slaves_2_araddr     : out std_logic_vector(63 downto 0);
    
    --    -- value method v_to_slaves_2_m_arlen
    --    v_to_slaves_2_arlen      : out std_logic_vector(7 downto 0);
    
    --    -- value method v_to_slaves_2_m_arsize
    --    v_to_slaves_2_arsize     : out std_logic_vector(2 downto 0);
    
    --    -- value method v_to_slaves_2_m_arburst
    --    v_to_slaves_2_arburst    : out std_logic_vector(1 downto 0);
    
    --    -- value method v_to_slaves_2_m_arlock
    --    v_to_slaves_2_arlock     : out std_logic;
    
    --    -- value method v_to_slaves_2_m_arcache
    --    v_to_slaves_2_arcache    : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_2_m_arprot
    --    v_to_slaves_2_arprot     : out std_logic_vector(2 downto 0);
    
    --    -- value method v_to_slaves_2_m_arqos
    --    v_to_slaves_2_arqos      : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_2_m_arregion
    --    v_to_slaves_2_arregion   : out std_logic_vector(3 downto 0);
    
    --    -- value method v_to_slaves_2_m_aruser
    
    --    -- action method v_to_slaves_2_m_arready
    --    v_to_slaves_2_arready    : in std_logic;
    
    --    -- action method v_to_slaves_2_m_rvalid
    --    v_to_slaves_2_rvalid     : in std_logic;
    --    v_to_slaves_2_rid        : in std_logic_vector(3 downto 0);
    --    v_to_slaves_2_rdata      : in std_logic_vector(63 downto 0);
    --    v_to_slaves_2_rresp      : in std_logic_vector(1 downto 0);
    --    v_to_slaves_2_rlast      : in std_logic;
    
    --    -- value method v_to_slaves_2_m_rready
    --    v_to_slaves_2_rready     : out std_logic
    --);
    --end component;

    --component axi_uartlite_0
    --port (
    --    s_axi_aclk   : in std_logic;
    --    s_axi_aresetn: in std_logic;
    --    interrupt    : out std_logic;
    --    s_axi_awaddr : in std_logic_vector(3 downto 0);
    --    s_axi_awvalid: in std_logic;
    --    s_axi_awready: out std_logic;
    --    s_axi_wdata  : in std_logic_vector(31 downto 0);
    --    s_axi_wstrb  : in std_logic_vector(3 downto 0);
    --    s_axi_wvalid : in std_logic;
    --    s_axi_wready : out std_logic;
    --    s_axi_bresp  : out std_logic_vector(1 downto 0);
    --    s_axi_bvalid : out std_logic;
    --    s_axi_bready : in std_logic;
    --    s_axi_araddr : in std_logic_vector(3 downto 0);
    --    s_axi_arvalid: in std_logic;
    --    s_axi_arready: out std_logic;
    --    s_axi_rdata  : out std_logic_vector(31 downto 0);
    --    s_axi_rresp  : out std_logic_vector(1 downto 0);
    --    s_axi_rvalid : out std_logic;
    --    s_axi_rready : in std_logic;
    --    rx           : in std_logic;
    --    tx           : out std_logic
    --);
    --end component;

    begin

    flute_rv32AC : mkSoC_Top
    port map (
        CLK                           => CLK100MHZ,
        RST_N                         => CPU_RESETN,
    
        EN_to_raw_mem_request_get     => CLK100MHZ,
        to_raw_mem_request_get        => open,
        RDY_to_raw_mem_request_get    => open,
    
        to_raw_mem_response_put       => (others => '0'),
        EN_to_raw_mem_response_put    => CLK100MHZ,
        RDY_to_raw_mem_response_put   => open,
    
        EN_get_to_console_get         => CLK100MHZ,
        get_to_console_get            => LED(15 downto 8),
        RDY_get_to_console_get        => open,
    
        put_from_console_put          => (others => '0'),
        EN_put_from_console_put       => CLK100MHZ,
        RDY_put_from_console_put      => open,
    
        status                        => LED(7 downto 0),
    
        set_verbosity_verbosity       => (others => '0'),
        set_verbosity_logdelay        => (others => '0'),
        EN_set_verbosity              => CLK100MHZ,
        RDY_set_verbosity             => open,
    
        set_watch_tohost_watch_tohost => CLK100MHZ,
        set_watch_tohost_tohost_addr  => (others => '0'),
        EN_set_watch_tohost           => CLK100MHZ,
        RDY_set_watch_tohost          => open,
    
        mv_tohost_value               => open,
        RDY_mv_tohost_value           => open,
    
        EN_ma_ddr4_ready              => CLK100MHZ,
        RDY_ma_ddr4_ready             => open,
    
        mv_status                     => open
        );

  --  flute_rv32AC : mkCore
  --  port map(
  --      CLK                                                             => CLK100MHZ,
  --      RST_N                                                           => CPU_RESETN,
    
  --      -- action method cpu_reset_server_request_put
  --      cpu_reset_server_request_put                                    => '0',
  --      EN_cpu_reset_server_request_put                                 => '0',
  --      RDY_cpu_reset_server_request_put                                => open,
    
  --      -- actionvalue method cpu_reset_server_response_get
  --      EN_cpu_reset_server_response_get                                => '0',
  --      cpu_reset_server_response_get                                   => open,
  --      RDY_cpu_reset_server_response_get                               => open,
    
  --      -- value method cpu_imem_master_m_awvalid
  --      cpu_imem_master_awvalid                                         => open,
    
  --      -- value method cpu_imem_master_m_awid
  --      cpu_imem_master_awid                                            => open,
    
  --      -- value method cpu_imem_master_m_awaddr
  --      cpu_imem_master_awaddr                                          => open,
    
  --      -- value method cpu_imem_master_m_awlen
  --      cpu_imem_master_awlen                                           => open,
    
  --      -- value method cpu_imem_master_m_awsize
  --      cpu_imem_master_awsize                                          => open,
    
  --      -- value method cpu_imem_master_m_awburst
  --      cpu_imem_master_awburst                                         => open,
    
  --      -- value method cpu_imem_master_m_awlock
  --      cpu_imem_master_awlock                                          => open,
    
  --      -- value method cpu_imem_master_m_awcache
  --      cpu_imem_master_awcache                                         => open,
    
  --      -- value method cpu_imem_master_m_awprot
  --      cpu_imem_master_awprot                                          => open,
    
  --      -- value method cpu_imem_master_m_awqos
  --      cpu_imem_master_awqos                                           => open,
    
  --      -- value method cpu_imem_master_m_awregion
  --      cpu_imem_master_awregion                                        => open,
    
  --      -- value method cpu_imem_master_m_awuser
    
  --      -- action method cpu_imem_master_m_awready
  --      cpu_imem_master_awready                                         => '0',
    
  --      -- value method cpu_imem_master_m_wvalid
  --      cpu_imem_master_wvalid                                          => open,
    
  --      -- value method cpu_imem_master_m_wdata
  --      cpu_imem_master_wdata                                           => open,
    
  --      -- value method cpu_imem_master_m_wstrb
  --      cpu_imem_master_wstrb                                           => open,
    
  --      -- value method cpu_imem_master_m_wlast
  --      cpu_imem_master_wlast                                           => open,
    
  --      -- value method cpu_imem_master_m_wuser
    
  --      -- action method cpu_imem_master_m_wready
  --      cpu_imem_master_wready                                          => '0',
    
  --      -- action method cpu_imem_master_m_bvalid
  --      cpu_imem_master_bvalid                                          => '0',
  --      cpu_imem_master_bid                                             => (others => '0'),
  --      cpu_imem_master_bresp                                           => (others => '0'),
    
  --      -- value method cpu_imem_master_m_bready
  --      cpu_imem_master_bready                                          => open,
    
  --      -- value method cpu_imem_master_m_arvalid
  --      cpu_imem_master_arvalid                                         => open,
    
  --      -- value method cpu_imem_master_m_arid
  --      cpu_imem_master_arid                                            => open,
    
  --      -- value method cpu_imem_master_m_araddr
  --      cpu_imem_master_araddr                                          => open,
    
  --      -- value method cpu_imem_master_m_arlen
  --      cpu_imem_master_arlen                                           => open,
    
  --      -- value method cpu_imem_master_m_arsize
  --      cpu_imem_master_arsize                                          => open,
    
  --      -- value method cpu_imem_master_m_arburst
  --      cpu_imem_master_arburst                                         => open,
    
  --      -- value method cpu_imem_master_m_arlock
  --      cpu_imem_master_arlock                                          => open,
    
  --      -- value method cpu_imem_master_m_arcache
  --      cpu_imem_master_arcache                                         => open,
    
  --      -- value method cpu_imem_master_m_arprot
  --      cpu_imem_master_arprot                                          => open,
    
  --      -- value method cpu_imem_master_m_arqos
  --      cpu_imem_master_arqos                                           => open,
    
  --      -- value method cpu_imem_master_m_arregion
  --      cpu_imem_master_arregion                                        => open,
    
  --      -- value method cpu_imem_master_m_aruser
    
  --      -- action method cpu_imem_master_m_arready
  --      cpu_imem_master_arready                                         => '0',
    
  --      -- action method cpu_imem_master_m_rvalid
  --      cpu_imem_master_rvalid                                          => '0',
  --      cpu_imem_master_rid                                             => (others => '0'),
  --      cpu_imem_master_rdata                                           => (others => '0'),
  --      cpu_imem_master_rresp                                           => (others => '0'),
  --      cpu_imem_master_rlast                                           => '0',
    
  --      -- value method cpu_imem_master_m_rready
  --      cpu_imem_master_rready                                          => open,
    
  --      -- value method core_mem_master_m_awvalid
  --      core_mem_master_awvalid                                         => open,
    
  --      -- value method core_mem_master_m_awid
  --      core_mem_master_awid                                            => open,
    
  --      -- value method core_mem_master_m_awaddr
  --      core_mem_master_awaddr                                          => open,
    
  --      -- value method core_mem_master_m_awlen
  --      core_mem_master_awlen                                           => open,
    
  --      -- value method core_mem_master_m_awsize
  --      core_mem_master_awsize                                          => open,
    
  --      -- value method core_mem_master_m_awburst
  --      core_mem_master_awburst                                         => open,
    
  --      -- value method core_mem_master_m_awlock
  --      core_mem_master_awlock                                          => open,
    
  --      -- value method core_mem_master_m_awcache
  --      core_mem_master_awcache                                         => open,
    
  --      -- value method core_mem_master_m_awprot
  --      core_mem_master_awprot                                          => open,
    
  --      -- value method core_mem_master_m_awqos
  --      core_mem_master_awqos                                           => open,
    
  --      -- value method core_mem_master_m_awregion
  --      core_mem_master_awregion                                        => open,
    
  --      -- value method core_mem_master_m_awuser
    
  --      -- action method core_mem_master_m_awready
  --      core_mem_master_awready                                         => '0',
    
  --      -- value method core_mem_master_m_wvalid
  --      core_mem_master_wvalid                                          => open,
    
  --      -- value method core_mem_master_m_wdata
  --      core_mem_master_wdata                                           => open,
    
  --      -- value method core_mem_master_m_wstrb
  --      core_mem_master_wstrb                                           => open,
    
  --      -- value method core_mem_master_m_wlast
  --      core_mem_master_wlast                                           => open,
    
  --      -- value method core_mem_master_m_wuser
    
  --      -- action method core_mem_master_m_wready
  --      core_mem_master_wready                                          => '0',
    
  --      -- action method core_mem_master_m_bvalid
  --      core_mem_master_bvalid                                          => '0',
  --      core_mem_master_bid                                             => (others => '0'),
  --      core_mem_master_bresp                                           => (others => '0'),
    
  --      -- value method core_mem_master_m_bready
  --      core_mem_master_bready                                          => open,
    
  --      -- value method core_mem_master_m_arvalid
  --      core_mem_master_arvalid                                         => open,
    
  --      -- value method core_mem_master_m_arid
  --      core_mem_master_arid                                            => open,
    
  --      -- value method core_mem_master_m_araddr
  --      core_mem_master_araddr                                          => open,
    
  --      -- value method core_mem_master_m_arlen
  --      core_mem_master_arlen                                           => open,
    
  --      -- value method core_mem_master_m_arsize
  --      core_mem_master_arsize                                          => open,
    
  --      -- value method core_mem_master_m_arburst
  --      core_mem_master_arburst                                         => open,
    
  --      -- value method core_mem_master_m_arlock
  --      core_mem_master_arlock                                          => open,
    
  --      -- value method core_mem_master_m_arcache
  --      core_mem_master_arcache                                         => open,
    
  --      -- value method core_mem_master_m_arprot
  --      core_mem_master_arprot                                          => open,
    
  --      -- value method core_mem_master_m_arqos
  --      core_mem_master_arqos                                           => open,
    
  --      -- value method core_mem_master_m_arregion
  --      core_mem_master_arregion                                        => open,
    
  --      -- value method core_mem_master_m_aruser
    
  --      -- action method core_mem_master_m_arready
  --      core_mem_master_arready                                         => '0',
    
  --      -- action method core_mem_master_m_rvalid
  --      core_mem_master_rvalid                                          => '0',
  --      core_mem_master_rid                                             => (others => '0'),
  --      core_mem_master_rdata                                           => (others => '0'),
  --      core_mem_master_rresp                                           => (others => '0'),
  --      core_mem_master_rlast                                           => '0',
    
  --      -- value method core_mem_master_m_rready
  --      core_mem_master_rready                                          => open,
    
  --      -- action method dma_server_m_awvalid
  --      dma_server_awvalid                                              => '0',
  --      dma_server_awid                                                 => (others => '0'),
  --      dma_server_awaddr                                               => (others => '0'),
  --      dma_server_awlen                                                => (others => '0'),
  --      dma_server_awsize                                               => (others => '0'),
  --      dma_server_awburst                                              => (others => '0'),
  --      dma_server_awlock                                               => '0',
  --      dma_server_awcache                                              => (others => '0'),
  --      dma_server_awprot                                               => (others => '0'),
  --      dma_server_awqos                                                => (others => '0'),
  --      dma_server_awregion                                             => (others => '0'),
    
  --      -- value method dma_server_m_awready
  --      dma_server_awready                                              => open,
    
  --      -- action method dma_server_m_wvalid
  --      dma_server_wvalid                                               => '0',
  --      dma_server_wdata                                                => (others => '0'),
  --      dma_server_wstrb                                                => (others => '0'),
  --      dma_server_wlast                                                => '0',
    
  --      -- value method dma_server_m_wready
  --      dma_server_wready                                               => open,
    
  --      -- value method dma_server_m_bvalid
  --      dma_server_bvalid                                               => open,
    
  --      -- value method dma_server_m_bid
  --      dma_server_bid                                                  => open,
    
  --      -- value method dma_server_m_bresp
  --      dma_server_bresp                                                => open,
    
  --      -- value method dma_server_m_buser
    
  --      -- action method dma_server_m_bready
  --      dma_server_bready                                               => '0',
    
  --      -- action method dma_server_m_arvalid
  --      dma_server_arvalid                                              => '0',
  --      dma_server_arid                                                 => (others => '0'),
  --      dma_server_araddr                                               => (others => '0'),
  --      dma_server_arlen                                                => (others => '0'),
  --      dma_server_arsize                                               => (others => '0'),
  --      dma_server_arburst                                              => (others => '0'),
  --      dma_server_arlock                                               => '0',
  --      dma_server_arcache                                              => (others => '0'),
  --      dma_server_arprot                                               => (others => '0'),
  --      dma_server_arqos                                                => (others => '0'),
  --      dma_server_arregion                                             => (others => '0'),
    
  --      -- value method dma_server_m_arready
  --      dma_server_arready                                              => open,
    
  --      -- value method dma_server_m_rvalid
  --      dma_server_rvalid                                               => open,
    
  --      -- value method dma_server_m_rid
  --      dma_server_rid                                                  => open,
    
  --      -- value method dma_server_m_rdata
  --      dma_server_rdata                                                => open,
    
  --      -- value method dma_server_m_rresp
  --      dma_server_rresp                                                => open,
    
  --      -- value method dma_server_m_rlast
  --      dma_server_rlast                                                => open,
    
  --      -- value method dma_server_m_ruser
    
  --      -- action method dma_server_m_rready
  --      dma_server_rready                                               => '0',
    
  --      -- action method core_external_interrupt_sources_0_m_interrupt_req
  --      core_external_interrupt_sources_0_m_interrupt_req_set_not_clear => '0',
    
  --      -- action method core_external_interrupt_sources_1_m_interrupt_req
  --      core_external_interrupt_sources_1_m_interrupt_req_set_not_clear => '0',
    
  --      -- action method core_external_interrupt_sources_2_m_interrupt_req
  --      core_external_interrupt_sources_2_m_interrupt_req_set_not_clear => '0',
    
  --      -- action method core_external_interrupt_sources_3_m_interrupt_req
  --      core_external_interrupt_sources_3_m_interrupt_req_set_not_clear => '0',
    
  --      -- action method core_external_interrupt_sources_4_m_interrupt_req
  --      core_external_interrupt_sources_4_m_interrupt_req_set_not_clear => '0',
    
  --      -- action method core_external_interrupt_sources_5_m_interrupt_req
  --      core_external_interrupt_sources_5_m_interrupt_req_set_not_clear => '0',
    
  --      -- action method core_external_interrupt_sources_6_m_interrupt_req
  --      core_external_interrupt_sources_6_m_interrupt_req_set_not_clear => '0',
    
  --      -- action method core_external_interrupt_sources_7_m_interrupt_req
  --      core_external_interrupt_sources_7_m_interrupt_req_set_not_clear => '0',
    
  --      -- action method core_external_interrupt_sources_8_m_interrupt_req
  --      core_external_interrupt_sources_8_m_interrupt_req_set_not_clear => '0',
    
  --      -- action method core_external_interrupt_sources_9_m_interrupt_req
  --      core_external_interrupt_sources_9_m_interrupt_req_set_not_clear => '0',
    
  --      -- action method core_external_interrupt_sources_10_m_interrupt_req
  --      core_external_interrupt_sources_10_m_interrupt_req_set_not_clear=> '0',
    
  --      -- action method core_external_interrupt_sources_11_m_interrupt_req
  --      core_external_interrupt_sources_11_m_interrupt_req_set_not_clear=> '0',
    
  --      -- action method core_external_interrupt_sources_12_m_interrupt_req
  --      core_external_interrupt_sources_12_m_interrupt_req_set_not_clear=> '0',
    
  --      -- action method core_external_interrupt_sources_13_m_interrupt_req
  --      core_external_interrupt_sources_13_m_interrupt_req_set_not_clear=> '0',
    
  --      -- action method core_external_interrupt_sources_14_m_interrupt_req
  --      core_external_interrupt_sources_14_m_interrupt_req_set_not_clear=> '0',
    
  --      -- action method core_external_interrupt_sources_15_m_interrupt_req
  --      core_external_interrupt_sources_15_m_interrupt_req_set_not_clear=> '0',
    
  --      -- action method nmi_req
  --      nmi_req_set_not_clear                                           => '0',
    
  --      -- action method set_verbosity
  --      set_verbosity_verbosity                                         => (others => '0'),
  --      set_verbosity_logdelay                                          => (others => '0'),
  --      EN_set_verbosity                                                => '0',
  --      RDY_set_verbosity                                               => open,
    
  --      -- action method set_watch_tohost
  --      set_watch_tohost_watch_tohost                                   => '0',
  --      set_watch_tohost_tohost_addr                                    => (others => '0'),
  --      EN_set_watch_tohost                                             => '0',
  --      RDY_set_watch_tohost                                            => open,
    
  --      -- value method mv_tohost_value
  --      mv_tohost_value                                                 => open,
  --      RDY_mv_tohost_value                                             => open,
    
  --      -- action method ma_ddr4_ready
  --      EN_ma_ddr4_ready                                                => '0',
  --      RDY_ma_ddr4_ready                                               => open,
    
  --      -- value method mv_status
  --      mv_status                                                       => open
  --  );

  --  -- TODO - Connect AXI master to AXI crossbar.
  --  AXI4_CROSSBAR : mkFabric_AXI4
  --  port map(
  --      CLK                      => CLK100MHZ,
  --      RST_N                    => CPU_RESETN,

  --      -- action method reset
  --      EN_reset                 => '0',
  --      RDY_reset                => open,

  --      -- action method set_verbosity
  --      set_verbosity_verbosity  => (others => '0'),
  --      EN_set_verbosity         => '0',
  --      RDY_set_verbosity        => open,

  --      -- action method v_from_masters_0_m_awvalid
  --      v_from_masters_0_awvalid => '0',
  --      v_from_masters_0_awid    => (others => '0'),
  --      v_from_masters_0_awaddr  => (others => '0'),
  --      v_from_masters_0_awlen   => (others => '0'),
  --      v_from_masters_0_awsize  => (others => '0'),
  --      v_from_masters_0_awburst => (others => '0'),
  --      v_from_masters_0_awlock  => '0',
  --      v_from_masters_0_awcache => (others => '0'),
  --      v_from_masters_0_awprot  => (others => '0'),
  --      v_from_masters_0_awqos   => (others => '0'),
  --      v_from_masters_0_awregion=> (others => '0'),

  --      -- value method v_from_masters_0_m_awready
  --      v_from_masters_0_awready => open,

  --      -- action method v_from_masters_0_m_wvalid
  --      v_from_masters_0_wvalid  => '0',
  --      v_from_masters_0_wdata   => (others => '0'),
  --      v_from_masters_0_wstrb   => (others => '0'),
  --      v_from_masters_0_wlast   => '0',

  --      -- value method v_from_masters_0_m_wready
  --      v_from_masters_0_wready  => open,

  --      -- value method v_from_masters_0_m_bvalid
  --      v_from_masters_0_bvalid  => open,

  --      -- value method v_from_masters_0_m_bid
  --      v_from_masters_0_bid     => open,

  --      -- value method v_from_masters_0_m_bresp
  --      v_from_masters_0_bresp   => open,

  --      -- value method v_from_masters_0_m_buser

  --      -- action method v_from_masters_0_m_bready
  --      v_from_masters_0_bready  => '0',

  --      -- action method v_from_masters_0_m_arvalid
  --      v_from_masters_0_arvalid => '0',
  --      v_from_masters_0_arid    => (others => '0'),
  --      v_from_masters_0_araddr  => (others => '0'),
  --      v_from_masters_0_arlen   => (others => '0'),
  --      v_from_masters_0_arsize  => (others => '0'),
  --      v_from_masters_0_arburst => (others => '0'),
  --      v_from_masters_0_arlock  => '0',
  --      v_from_masters_0_arcache => (others => '0'),
  --      v_from_masters_0_arprot  => (others => '0'),
  --      v_from_masters_0_arqos   => (others => '0'),
  --      v_from_masters_0_arregion=> (others => '0'),

  --      -- value method v_from_masters_0_m_arready
  --      v_from_masters_0_arready => open,

  --      -- value method v_from_masters_0_m_rvalid
  --      v_from_masters_0_rvalid  => open,

  --      -- value method v_from_masters_0_m_rid
  --      v_from_masters_0_rid     => open,

  --      -- value method v_from_masters_0_m_rdata
  --      v_from_masters_0_rdata   => open,

  --      -- value method v_from_masters_0_m_rresp
  --      v_from_masters_0_rresp   => open,

  --      -- value method v_from_masters_0_m_rlast
  --      v_from_masters_0_rlast   => open,

  --      -- value method v_from_masters_0_m_ruser

  --      -- action method v_from_masters_0_m_rready
  --      v_from_masters_0_rready  => '0',

  --      -- action method v_from_masters_1_m_awvalid
  --      v_from_masters_1_awvalid => '0',
  --      v_from_masters_1_awid    => (others => '0'),
  --      v_from_masters_1_awaddr  => (others => '0'),
  --      v_from_masters_1_awlen   => (others => '0'),
  --      v_from_masters_1_awsize  => (others => '0'),
  --      v_from_masters_1_awburst => (others => '0'),
  --      v_from_masters_1_awlock  => '0',
  --      v_from_masters_1_awcache => (others => '0'),
  --      v_from_masters_1_awprot  => (others => '0'),
  --      v_from_masters_1_awqos   => (others => '0'),
  --      v_from_masters_1_awregion=> (others => '0'),

  --      -- value method v_from_masters_1_m_awready
  --      v_from_masters_1_awready => open,

  --      -- action method v_from_masters_1_m_wvalid
  --      v_from_masters_1_wvalid  => '0',
  --      v_from_masters_1_wdata   => (others => '0'),
  --      v_from_masters_1_wstrb   => (others => '0'),
  --      v_from_masters_1_wlast   => '0',

  --      -- value method v_from_masters_1_m_wready
  --      v_from_masters_1_wready  => open,

  --      -- value method v_from_masters_1_m_bvalid
  --      v_from_masters_1_bvalid  => open,

  --      -- value method v_from_masters_1_m_bid
  --      v_from_masters_1_bid     => open,

  --      -- value method v_from_masters_1_m_bresp
  --      v_from_masters_1_bresp   => open,

  --      -- value method v_from_masters_1_m_buser

  --      -- action method v_from_masters_1_m_bready
  --      v_from_masters_1_bready  => '0',

  --      -- action method v_from_masters_1_m_arvalid
  --      v_from_masters_1_arvalid => '0',
  --      v_from_masters_1_arid    => (others => '0'),
  --      v_from_masters_1_araddr  => (others => '0'),
  --      v_from_masters_1_arlen   => (others => '0'),
  --      v_from_masters_1_arsize  => (others => '0'),
  --      v_from_masters_1_arburst => (others => '0'),
  --      v_from_masters_1_arlock  => '0',
  --      v_from_masters_1_arcache => (others => '0'),
  --      v_from_masters_1_arprot  => (others => '0'),
  --      v_from_masters_1_arqos   => (others => '0'),
  --      v_from_masters_1_arregion=> (others => '0'),

  --      -- value method v_from_masters_1_m_arready
  --      v_from_masters_1_arready => open,

  --      -- value method v_from_masters_1_m_rvalid
  --      v_from_masters_1_rvalid  => open,

  --      -- value method v_from_masters_1_m_rid
  --      v_from_masters_1_rid     => open,

  --      -- value method v_from_masters_1_m_rdata
  --      v_from_masters_1_rdata   => open,

  --      -- value method v_from_masters_1_m_rresp
  --      v_from_masters_1_rresp   => open,

  --      -- value method v_from_masters_1_m_rlast
  --      v_from_masters_1_rlast   => open,

  --      -- value method v_from_masters_1_m_ruser

  --      -- action method v_from_masters_1_m_rready
  --      v_from_masters_1_rready  => '0',

  --      -- value method v_to_slaves_0_m_awvalid
  --      v_to_slaves_0_awvalid    => open,

  --      -- value method v_to_slaves_0_m_awid
  --      v_to_slaves_0_awid       => open,

  --      -- value method v_to_slaves_0_m_awaddr
  --      v_to_slaves_0_awaddr     => open,

  --      -- value method v_to_slaves_0_m_awlen
  --      v_to_slaves_0_awlen      => open,

  --      -- value method v_to_slaves_0_m_awsize
  --      v_to_slaves_0_awsize     => open,

  --      -- value method v_to_slaves_0_m_awburst
  --      v_to_slaves_0_awburst    => open,

  --      -- value method v_to_slaves_0_m_awlock
  --      v_to_slaves_0_awlock     => open,

  --      -- value method v_to_slaves_0_m_awcache
  --      v_to_slaves_0_awcache    => open,

  --      -- value method v_to_slaves_0_m_awprot
  --      v_to_slaves_0_awprot     => open,

  --      -- value method v_to_slaves_0_m_awqos
  --      v_to_slaves_0_awqos      => open,

  --      -- value method v_to_slaves_0_m_awregion
  --      v_to_slaves_0_awregion   => open,

  --      -- value method v_to_slaves_0_m_awuser

  --      -- action method v_to_slaves_0_m_awready
  --      v_to_slaves_0_awready    => '0',

  --      -- value method v_to_slaves_0_m_wvalid
  --      v_to_slaves_0_wvalid     => open,

  --      -- value method v_to_slaves_0_m_wdata
  --      v_to_slaves_0_wdata      => open,

  --      -- value method v_to_slaves_0_m_wstrb
  --      v_to_slaves_0_wstrb      => open,

  --      -- value method v_to_slaves_0_m_wlast
  --      v_to_slaves_0_wlast      => open,

  --      -- value method v_to_slaves_0_m_wuser

  --      -- action method v_to_slaves_0_m_wready
  --      v_to_slaves_0_wready     => '0',

  --      -- action method v_to_slaves_0_m_bvalid
  --      v_to_slaves_0_bvalid     => '0',
  --      v_to_slaves_0_bid        => (others => '0'),
  --      v_to_slaves_0_bresp      => (others => '0'),

  --      -- value method v_to_slaves_0_m_bready
  --      v_to_slaves_0_bready     => open,

  --      -- value method v_to_slaves_0_m_arvalid
  --      v_to_slaves_0_arvalid    => open,

  --      -- value method v_to_slaves_0_m_arid
  --      v_to_slaves_0_arid       => open,

  --      -- value method v_to_slaves_0_m_araddr
  --      v_to_slaves_0_araddr     => open,

  --      -- value method v_to_slaves_0_m_arlen
  --      v_to_slaves_0_arlen      => open,

  --      -- value method v_to_slaves_0_m_arsize
  --      v_to_slaves_0_arsize     => open,

  --      -- value method v_to_slaves_0_m_arburst
  --      v_to_slaves_0_arburst    => open,

  --      -- value method v_to_slaves_0_m_arlock
  --      v_to_slaves_0_arlock     => open,

  --      -- value method v_to_slaves_0_m_arcache
  --      v_to_slaves_0_arcache    => open,

  --      -- value method v_to_slaves_0_m_arprot
  --      v_to_slaves_0_arprot     => open,

  --      -- value method v_to_slaves_0_m_arqos
  --      v_to_slaves_0_arqos      => open,

  --      -- value method v_to_slaves_0_m_arregion
  --      v_to_slaves_0_arregion   => open,

  --      -- value method v_to_slaves_0_m_aruser

  --      -- action method v_to_slaves_0_m_arready
  --      v_to_slaves_0_arready    => '0',

  --      -- action method v_to_slaves_0_m_rvalid
  --      v_to_slaves_0_rvalid     => '0',
  --      v_to_slaves_0_rid        => (others => '0'),
  --      v_to_slaves_0_rdata      => (others => '0'),
  --      v_to_slaves_0_rresp      => (others => '0'),
  --      v_to_slaves_0_rlast      => '0',

  --      -- value method v_to_slaves_0_m_rready
  --      v_to_slaves_0_rready     => open,

  --      -- value method v_to_slaves_1_m_awvalid
  --      v_to_slaves_1_awvalid    => open,

  --      -- value method v_to_slaves_1_m_awid
  --      v_to_slaves_1_awid       => open,

  --      -- value method v_to_slaves_1_m_awaddr
  --      v_to_slaves_1_awaddr     => open,

  --      -- value method v_to_slaves_1_m_awlen
  --      v_to_slaves_1_awlen      => open,

  --      -- value method v_to_slaves_1_m_awsize
  --      v_to_slaves_1_awsize     => open,

  --      -- value method v_to_slaves_1_m_awburst
  --      v_to_slaves_1_awburst    => open,

  --      -- value method v_to_slaves_1_m_awlock
  --      v_to_slaves_1_awlock     => open,

  --      -- value method v_to_slaves_1_m_awcache
  --      v_to_slaves_1_awcache    => open,

  --      -- value method v_to_slaves_1_m_awprot
  --      v_to_slaves_1_awprot     => open,

  --      -- value method v_to_slaves_1_m_awqos
  --      v_to_slaves_1_awqos      => open,

  --      -- value method v_to_slaves_1_m_awregion
  --      v_to_slaves_1_awregion   => open,

  --      -- value method v_to_slaves_1_m_awuser

  --      -- action method v_to_slaves_1_m_awready
  --      v_to_slaves_1_awready    => '0',

  --      -- value method v_to_slaves_1_m_wvalid
  --      v_to_slaves_1_wvalid     => open,

  --      -- value method v_to_slaves_1_m_wdata
  --      v_to_slaves_1_wdata      => open,

  --      -- value method v_to_slaves_1_m_wstrb
  --      v_to_slaves_1_wstrb      => open,

  --      -- value method v_to_slaves_1_m_wlast
  --      v_to_slaves_1_wlast      => open,

  --      -- value method v_to_slaves_1_m_wuser

  --      -- action method v_to_slaves_1_m_wready
  --      v_to_slaves_1_wready     => '0',

  --      -- action method v_to_slaves_1_m_bvalid
  --      v_to_slaves_1_bvalid     => '0',
  --      v_to_slaves_1_bid        => (others => '0'),
  --      v_to_slaves_1_bresp      => (others => '0'),

  --      -- value method v_to_slaves_1_m_bready
  --      v_to_slaves_1_bready     => open,

  --      -- value method v_to_slaves_1_m_arvalid
  --      v_to_slaves_1_arvalid    => open,

  --      -- value method v_to_slaves_1_m_arid
  --      v_to_slaves_1_arid       => open,

  --      -- value method v_to_slaves_1_m_araddr
  --      v_to_slaves_1_araddr     => open,

  --      -- value method v_to_slaves_1_m_arlen
  --      v_to_slaves_1_arlen      => open,

  --      -- value method v_to_slaves_1_m_arsize
  --      v_to_slaves_1_arsize     => open,

  --      -- value method v_to_slaves_1_m_arburst
  --      v_to_slaves_1_arburst    => open,

  --      -- value method v_to_slaves_1_m_arlock
  --      v_to_slaves_1_arlock     => open,

  --      -- value method v_to_slaves_1_m_arcache
  --      v_to_slaves_1_arcache    => open,

  --      -- value method v_to_slaves_1_m_arprot
  --      v_to_slaves_1_arprot     => open,

  --      -- value method v_to_slaves_1_m_arqos
  --      v_to_slaves_1_arqos      => open,

  --      -- value method v_to_slaves_1_m_arregion
  --      v_to_slaves_1_arregion   => open,

  --      -- value method v_to_slaves_1_m_aruser

  --      -- action method v_to_slaves_1_m_arready
  --      v_to_slaves_1_arready    => '0',

  --      -- action method v_to_slaves_1_m_rvalid
  --      v_to_slaves_1_rvalid     => '0',
  --      v_to_slaves_1_rid        => (others => '0'),
  --      v_to_slaves_1_rdata      => (others => '0'),
  --      v_to_slaves_1_rresp      => (others => '0'),
  --      v_to_slaves_1_rlast      => '0',

  --      -- value method v_to_slaves_1_m_rready
  --      v_to_slaves_1_rready     => open,

  --      -- value method v_to_slaves_2_m_awvalid
  --      v_to_slaves_2_awvalid    => open,

  --      -- value method v_to_slaves_2_m_awid
  --      v_to_slaves_2_awid       => open,

  --      -- value method v_to_slaves_2_m_awaddr
  --      v_to_slaves_2_awaddr     => open,

  --      -- value method v_to_slaves_2_m_awlen
  --      v_to_slaves_2_awlen      => open,

  --      -- value method v_to_slaves_2_m_awsize
  --      v_to_slaves_2_awsize     => open,

  --      -- value method v_to_slaves_2_m_awburst
  --      v_to_slaves_2_awburst    => open,

  --      -- value method v_to_slaves_2_m_awlock
  --      v_to_slaves_2_awlock     => open,

  --      -- value method v_to_slaves_2_m_awcache
  --      v_to_slaves_2_awcache    => open,

  --      -- value method v_to_slaves_2_m_awprot
  --      v_to_slaves_2_awprot     => open,

  --      -- value method v_to_slaves_2_m_awqos
  --      v_to_slaves_2_awqos      => open,

  --      -- value method v_to_slaves_2_m_awregion
  --      v_to_slaves_2_awregion   => open,

  --      -- value method v_to_slaves_2_m_awuser

  --      -- action method v_to_slaves_2_m_awready
  --      v_to_slaves_2_awready    => '0',

  --      -- value method v_to_slaves_2_m_wvalid
  --      v_to_slaves_2_wvalid     => open,

  --      -- value method v_to_slaves_2_m_wdata
  --      v_to_slaves_2_wdata      => open,

  --      -- value method v_to_slaves_2_m_wstrb
  --      v_to_slaves_2_wstrb      => open,

  --      -- value method v_to_slaves_2_m_wlast
  --      v_to_slaves_2_wlast      => open,

  --      -- value method v_to_slaves_2_m_wuser

  --      -- action method v_to_slaves_2_m_wready
  --      v_to_slaves_2_wready     => '0',

  --      -- action method v_to_slaves_2_m_bvalid
  --      v_to_slaves_2_bvalid     => '0',
  --      v_to_slaves_2_bid        => (others => '0'),
  --      v_to_slaves_2_bresp      => (others => '0'),

  --      -- value method v_to_slaves_2_m_bready
  --      v_to_slaves_2_bready     => open,

  --      -- value method v_to_slaves_2_m_arvalid
  --      v_to_slaves_2_arvalid    => open,

  --      -- value method v_to_slaves_2_m_arid
  --      v_to_slaves_2_arid       => open,

  --      -- value method v_to_slaves_2_m_araddr
  --      v_to_slaves_2_araddr     => open,

  --      -- value method v_to_slaves_2_m_arlen
  --      v_to_slaves_2_arlen      => open,

  --      -- value method v_to_slaves_2_m_arsize
  --      v_to_slaves_2_arsize     => open,

  --      -- value method v_to_slaves_2_m_arburst
  --      v_to_slaves_2_arburst    => open,

  --      -- value method v_to_slaves_2_m_arlock
  --      v_to_slaves_2_arlock     => open,

  --      -- value method v_to_slaves_2_m_arcache
  --      v_to_slaves_2_arcache    => open,

  --      -- value method v_to_slaves_2_m_arprot
  --      v_to_slaves_2_arprot     => open,

  --      -- value method v_to_slaves_2_m_arqos
  --      v_to_slaves_2_arqos      => open,

  --      -- value method v_to_slaves_2_m_arregion
  --      v_to_slaves_2_arregion   => open,

  --      -- value method v_to_slaves_2_m_aruser

  --      -- action method v_to_slaves_2_m_arready
  --      v_to_slaves_2_arready    => '0',

  --      -- action method v_to_slaves_2_m_rvalid
  --      v_to_slaves_2_rvalid     => '0',
  --      v_to_slaves_2_rid        => (others => '0'),
  --      v_to_slaves_2_rdata      => (others => '0'),
  --      v_to_slaves_2_rresp      => (others => '0'),
  --      v_to_slaves_2_rlast      => '0',

  --      -- value method v_to_slaves_2_m_rready
  --      v_to_slaves_2_rready     => open
  --);

  --  -- TODO - Connect UART IP to AXI slave.
  --  -- TODO - Connect UART IP to UART board IO.
  --  uart : axi_uartlite_0
  --  port map (
  --      s_axi_aclk    => CLK100MHZ,
  --      s_axi_aresetn => CPU_RESETN,
  --      interrupt     => open,
  --      s_axi_awaddr  => awaddr,
  --      s_axi_awvalid => awvalid,
  --      s_axi_awready => awready,
  --      s_axi_wdata   => wdata,
  --      s_axi_wstrb   => wstrb,
  --      s_axi_wvalid  => wvalid,
  --      s_axi_wready  => wready,
  --      s_axi_bresp   => bresp,
  --      s_axi_bvalid  => bvalid,
  --      s_axi_bready  => bready,
  --      s_axi_araddr  => araddr,
  --      s_axi_arvalid => arvalid,
  --      s_axi_arready => arready,
  --      s_axi_rdata   => rdata,
  --      s_axi_rresp   => rresp,
  --      s_axi_rvalid  => rvalid,
  --      s_axi_rready  => rready,
  --      rx            => rx,
  --      tx            => tx
  --  );

    end flute_arch;