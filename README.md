# Bluespec RISC-V Analysis

## Authors 

* Joshua Mack
* Samuel Mehalko

## Class

Computer Architecture
EN.525.612.82.SP22 

### Repository Notes

This repo contains submodules. To clone please run:

```bash
git clone --recursive -j $(nproc) <http/ssh link> 
```

## Building

NOTE: Please make sure vivado is added to $PATH
      before running the following makefile!
      export PATH=$PATH:/path/to/vivado/bin

To start building with gui:

```bash
make
```

To start building in terminal:

```bash
make gui=0
```

## Overview

Bluespec is a company that specializes in developing RISC-V processors. Many of their designs are made to be portable and support a wide range of FPGA targets.

The Bluespec Flute RISC-V processor with a 5-stage in-order pipeline. It supports both 32-bit or 64-bit operation and also contains support for virtual memory. The Flute CPU is configurable and can produce a CPU that supports RV32I up to RV64 capable of running Linux. 

The Bluespec MCU is one of their newest CPU designs which is meant to be a resource optimized RISC-V processor, mainly targeting Xilinx FPGA devices. The MCU design adheres to the RV32IM specification.

## Goals

1. Get Bluespec Compiler up and running
2. Implement Bluespec Flute RISC-V processor. Enable optional ISA M support to adhere to RV32IM spec.
3. Implement Bluespec MCU RISC-V processor.
4. Get both designs running on Xilinx Nexys A7 Eval Card.
5. Compare performance measurements for both Flute and MCU RISC processors (where applicable).
6. (Stretch) Get Linux running on softcore processor.

## Approach

The project will start by configuration of the Bluespec compiler. Once the compiler is functional, it will be possible to start standing up the reference designs for the Flute and MCU processors. The MCU will support RV32IM operation out of the box, but the Flute will need to be configured in order to add Multiply/Divide support as it defaults to a RV32I implementation.
Once the designs are implemented and built for the Nexys A7 Eval card, performance data will be measured. In order to do this, a piece of test software will be created and compiled to run on both CPUs. The performance of running this software will be compared to gather performance measurements on the CPU implementations.

## Resources

Bluespec provides reference designs for both the MCU and Flute designs. The design is implemented in Bluespec System Verilog (BSV), which utilizes the custom Bluespec compiler for compilation into synthesizable and simulatable Verilog.
Additionally, Xilinx Vivado will be utilized for synthesis and simulation as well as bitstream generation.
A Nexys A7 Eval card with a Xilinx XC7A100T-1CSG324C FPGA will be the target device for the designs.

* [Flute](https://github.com/bluespec/Flute)
* [MCU](https://bluespec.com/2021/09/27/bluespec-inc-releases-ultra-low-footprint-risc-v-processor-family-for-xilinx-fpgas-offers-free-quick-start-evaluation/)

## Expected Results

The Bluespec Flute RISC-V processor will contain a larger set of capabilities when compared to the MCU. However, the MCU uses a fraction of the resources which allows it to fit in much more resource constrained devices or designs. Due to this optimization, the Flute is expected to perform faster than the MCU in common performance tests since higher resource algorithms can be used if resources aren't as much of a concern.
However, despite the performance benefit of the Flute, the MCU may have a comparable performance despite a much lower resource utilization.

## Milestones

1. Get Compiler up and running (1 week) Mod 7
2. Get Flute Design up and running (2 weeks) Mod 8/9
3. Get MCU Design up and running (2 weeks) Mod 8/9
4. Status Report (1 weeks) Mod 9
5. Test Designs on Eval Card (1 week) Mod 10
6. Gather Performance Data (2 weeks) Mod 11/12
7. Final Report and Presentation Prep (2 weeks) Mod 11/12
8. Final Presentation (1 week) Mod 13