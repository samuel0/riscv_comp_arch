#!/usr/bin/tclsh

# Variable setup
set CPU_CORES [exec sh -c nproc]

# Create project.
create_project flute $::env(BUILD_DIR)/flute -part xc7a100tcsg324-1

# Files not needed but left should we switch to another version of the flute.
# add_file -norecurse $::env(SRC_DIR)/Flute/src_bsc_lib_RTL/BRAM1BE.v 
# add_file -norecurse $::env(SRC_DIR)/Flute/src_bsc_lib_RTL/BRAM1BELoad.v 
# add_file -norecurse $::env(SRC_DIR)/Flute/src_bsc_lib_RTL/BRAM2BE.v 
# add_file -norecurse $::env(SRC_DIR)/Flute/src_bsc_lib_RTL/BRAM2BELoad.v 
# add_file -norecurse $::env(SRC_DIR)/Flute/src_bsc_lib_RTL/FIFO1.v 
# add_file -norecurse $::env(SRC_DIR)/Flute/src_bsc_lib_RTL/FIFO10.v 
# add_file -norecurse $::env(SRC_DIR)/Flute/src_bsc_lib_RTL/FIFOL1.v 
# add_file -norecurse $::env(SRC_DIR)/Flute/src_bsc_lib_RTL/MakeResetA.v 
# add_file -norecurse $::env(SRC_DIR)/Flute/src_bsc_lib_RTL/RegFileLoad.v 
# add_file -norecurse $::env(SRC_DIR)/Flute/src_bsc_lib_RTL/RevertReg.v 
# add_file -norecurse $::env(SRC_DIR)/Flute/src_bsc_lib_RTL/SizedFIFO0.v 
# add_file -norecurse $::env(SRC_DIR)/Flute/src_bsc_lib_RTL/SyncFIFO.v 
# add_file -norecurse $::env(SRC_DIR)/Flute/src_bsc_lib_RTL/SyncFIFO0.v 
# add_file -norecurse $::env(SRC_DIR)/Flute/src_bsc_lib_RTL/SyncResetA.v 

# add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkFabric.v 
# add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkIntMul_32.v 
# add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkIntMul_64.v 
# add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkMem_Model.v 
# add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkNear_Mem_IO.v 
# add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkTop_HW_Side.v 

add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkAXI4_Deburster_A.v 
add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkBoot_ROM.v 
add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkBranch_Predictor.v 
add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkCore.v 
add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkCPU.v 
add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkCSR_MIE.v 
add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkCSR_MIP.v 
add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkCSR_RegFile.v 
add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkFabric_2x3.v 
add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkFabric_AXI4.v 
add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkGPR_RegFile.v 
add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkMem_Controller.v 
add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkMMU_Cache.v 
add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkNear_Mem.v
add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkNear_Mem_IO_AXI4.v 
add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkPLIC_16_2_7.v 
add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkRISCV_MBox.v 
add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkSoC_Map.v 
add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkSoC_Top.v 
add_file -norecurse $::env(SRC_DIR)/Flute/builds/RV32ACIMU_Flute_verilator/Verilog_RTL/mkUART.v 
add_file -norecurse $::env(SRC_DIR)/Flute/src_bsc_lib_RTL/BRAM2.v 
add_file -norecurse $::env(SRC_DIR)/Flute/src_bsc_lib_RTL/FIFO2.v 
add_file -norecurse $::env(SRC_DIR)/Flute/src_bsc_lib_RTL/FIFO20.v 
add_file -norecurse $::env(SRC_DIR)/Flute/src_bsc_lib_RTL/RegFile.v 
add_file -norecurse $::env(SRC_DIR)/Flute/src_bsc_lib_RTL/SizedFIFO.v 

# Add top level.
add_file -norecurse $::env(TOP_DIR)/flute.vhd 
add_files -fileset constrs_1 -norecurse $::env(SRC_DIR)/constraints/Nexys4DDR_Master.xdc
update_compile_order -fileset sources_1

# Generate UART IP core.
create_ip -name axi_uartlite -vendor xilinx.com -library ip -version 2.0 -module_name axi_uartlite_0
set_property -dict [list CONFIG.C_BAUDRATE {115200}] [get_ips axi_uartlite_0]
generate_target {instantiation_template} [get_files $::env(BUILD_DIR)/flute/flute.srcs/sources_1/ip/axi_uartlite_0/axi_uartlite_0.xci]
generate_target all [get_files  $::env(BUILD_DIR)/flute/flute.srcs/sources_1/ip/axi_uartlite_0/axi_uartlite_0.xci]
catch { config_ip_cache -export [get_ips -all axi_uartlite_0] }
export_ip_user_files -of_objects [get_files $::env(BUILD_DIR)/flute/flute.srcs/sources_1/ip/axi_uartlite_0/axi_uartlite_0.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] $::env(BUILD_DIR)/flute/flute.srcs/sources_1/ip/axi_uartlite_0/axi_uartlite_0.xci]
launch_runs axi_uartlite_0_synth_1 -jobs $CPU_CORES

# Set top level.
set_property top flute [current_fileset]
update_compile_order -fileset sources_1

# Run synthesis.
# TODO - Make this work.
# launch_runs synth_1 -jobs $CPU_CORES

# Run implementation.
# TODO - Make this work.
# launch_runs impl_1 -jobs $CPU_CORES