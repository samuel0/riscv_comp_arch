.ONESHELL:

# NOTE: Please make sure vivado is added to $PATH
#       before running the following makefile!
#       export PATH=$PATH:/path/to/vivado/bin

# Environment setup.
export BUILD_DIR=$(PWD)/build
export MAKE_DIR=$(PWD)/make
export SRC_DIR=$(PWD)/src
export TOP_DIR=$(PWD)/top
export SCRIPTS_DIR=$(MAKE_DIR)/scripts

# Default GUI to on.
gui ?= 1

ifeq ($(gui), 0)
    TCL_MODE="batch"
else
	TCL_MODE="gui"
endif

.SILENT: all
.PHONE: all
.DEFAULT: all
all: clean flute

# Directory clean up.
.PHONY: clean
.SILENT: clean
clean:
	kill -9 `pgrep vivado`
	if [ -d ${BUILD_DIR} ]; then
		rm -rf ${BUILD_DIR}
	fi

.PHONY: flute
.SILENT: flute
flute:
	echo --------------------
	echo -- Building flute --
	echo --------------------
	mkdir -p ${BUILD_DIR}
	cd ${BUILD_DIR}
	BUILD_DIR=${BUILD_DIR} vivado -mode $(TCL_MODE) -source $(SCRIPTS_DIR)/flute.tcl

.PHONY: mcu
.SILENT: mcu
mcu:
	echo --------------------
	echo -- Building MCU --
	echo --------------------
	mkdir -p ${BUILD_DIR}
	cd ${BUILD_DIR}
	${SRC_DIR}/MCU/utils/create_soc_project --project MCU --core RISCV32IM_MCU --platform nexys4ddr
