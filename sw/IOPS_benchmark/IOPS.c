// Simple C IOPS benchmark 
  
// Header file for input output functions
#include <stdio.h>
#include <time.h>
  
// main function -
// where the execution of program begins
int main()
{
  
    printf("Starting IOPS benchmark");
    clock_t start, end;
    start = clock();
    uint32_t var = 1;
    for(int idx = 0; idx < 10000000; idx++)
    {
       var=var+var;
    }
    end = clock();
    printf("\nStart clock = %d", start);
    printf("\nEnd clock = %d", end);
    //cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    //printf("\nTime: %f s\n",cpu_time_used);
    //double Flops=(ITERATIONS)/(double)(cpu_time_used);
  
    return 0;
}
