#include <stdio.h>

// for read_cycle
#include "riscv_counters.h"

// for PRIu64
#include <inttypes.h>

// For memory mapped IO
#include "soc_map.h"

int main (int argc, char ** argv)
{
   volatile unsigned char * gpio_addr = (void*)GPIO2_DATA;
   unsigned char pattern = 0;
   unsigned char iter = 0;
   unsigned bitpos = 0;
   unsigned int delay = 0;
   uint64_t start=read_cycle();

   *gpio_addr = 0x10;   // indicate SoT   // RGB-LED glows blue

   // A delay loop
   for (delay = 0; delay < (512*1024); delay++)
      __asm__ __volatile__ ("" ::: "memory");

   printf ("Starting LED sequence ... \n");

   // The LED loop
   for (iter=1; iter < 16; iter++) {
     // increment the gpio val
     (*gpio_addr)++;

     // Print the LED pattern
     printf("LED: ");

     for (bitpos=0; bitpos<4; bitpos++) {
        pattern = (iter >> bitpos) & 0x01;
        if (pattern == 0) printf (" - ");
        else              printf (" X ");
     }
     printf("\n");

     // A delay loop
     for (delay = 0; delay < (1024*1024); delay++)
        __asm__ __volatile__ ("" ::: "memory");
   }

   printf ("LED Sequence Complete. \n");
   uint64_t end=read_cycle();
   printf("read_cycle: %" PRIu64 " cycles have elapsed.\n",end-start);

   *gpio_addr = 0x20;   // indicate successful EoT // RGB-LED glows green


   // test BRAM slave example in documentation

   unsigned int * new_slave_addr = (void*)NEW_SLAVE_BASE;
   unsigned int i = 0;
   
   for (i = 0; i < (2048); i++) {
      // increment new_slave address, cycling through all 2K 32-bit words
      *(new_slave_addr + i) = 2*i + 1;
   }

   for (i = 0; i < (2048); i++) {
      // increment new_slave address, cycling through all 2K 32-bit words
      if (*(new_slave_addr + i) != 2*i + 1)
	printf("BRAM test: did not match at i = %d \n", i);
      else
	printf("BRAM test: match at i = %d \n", i);
      //	exit(1);
   }

   return (0);


}
