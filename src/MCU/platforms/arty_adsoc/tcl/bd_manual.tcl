################################################################
# START
################################################################


#
# Set default clock frequency (MHz) for the SoC subsystem and processor
set clock_freq_mhz 100

# 
# Set default processor IP
set proc_ip RV32IM_MCU

# 
# Set default device
set device xc7a100tcsg324-1


#set list_projs [get_projects -quiet]
#if { $list_projs eq "" } {
#   create_project project_1 myproj -part ${device}
#}
# # CHANGE DESIGN NAME HERE
# variable design_name
# set design_name ${platform}

# # If you do not already have an existing IP Integrator design open,
# # you can create a design using the following command:
# #    create_bd_design $design_name

# # Creating design if needed
# set errMsg ""
# set nRet 0

# set cur_design [current_bd_design -quiet]
# set list_cells [get_bd_cells -quiet]

# if { ${design_name} eq "" } {
#    # USE CASES:
#    #    1) Design_name not set

#    set errMsg "Please set the variable <design_name> to a non-empty value."
#    set nRet 1

# } elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
#    # USE CASES:
#    #    2): Current design opened AND is empty AND names same.
#    #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
#    #    4): Current design opened AND is empty AND names diff; design_name exists in project.

#    if { $cur_design ne $design_name } {
#       common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
#       set design_name [get_property NAME $cur_design]
#    }
#    common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

# } elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
#    # USE CASES:
#    #    5) Current design opened AND has components AND same names.

#    set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
#    set nRet 1
# } elseif { [get_files -quiet ${design_name}.bd] ne "" } {
#    # USE CASES:
#    #    6) Current opened design, has components, but diff names, design_name exists in project.
#    #    7) No opened design, design_name exists in project.

#    set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
#    set nRet 2

# } else {
#    # USE CASES:
#    #    8) No opened design, design_name not in project.
#    #    9) Current opened design, has components, but diff names, design_name not in project.

#    common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

#    create_bd_design $design_name

#    common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
#    current_bd_design $design_name

# }

# common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

# if { $nRet != 0 } {
#    catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
#    return $nRet
# }

# set bCheckIPsPassed 1
# ##################################################################
# # CHECK IPs
# ##################################################################
# set bCheckIPs 1
# if { $bCheckIPs == 1 } {
    set list_check_ips {xilinx.com:ip:axi_gpio:2.0}
    lappend list_check_ips xilinx.com:ip:axi_uart16550:2.0
    lappend list_check_ips xilinx.com:ip:proc_sys_reset:5.0
    lappend list_check_ips xilinx.com:ip:xlconstant:1.1
    lappend list_check_ips xilinx.com:ip:xlconcat:2.1
    lappend list_check_ips bluespec.com:ip:$proc_ip:E.1.0
    lappend list_check_ips bluespec.com:ip:xilinx_jtag:1.0

#    set list_ips_missing ""
#    common::send_msg_id "BD_TCL-006" "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

#    foreach ip_vlnv $list_check_ips {
#       set ip_obj [get_ipdefs -all $ip_vlnv]
#       if { $ip_obj eq "" } {
#          lappend list_ips_missing $ip_vlnv
#       }
#    }

#    if { $list_ips_missing ne "" } {
#       catch {common::send_msg_id "BD_TCL-115" "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
#       set bCheckIPsPassed 0
#    }

# }

# if { $bCheckIPsPassed != 1 } {
#   common::send_msg_id "BD_TCL-1003" "WARNING" "Will not continue with creation of design due to the error(s) above."
#   return 3
# }

##################################################################
# DESIGN PROCs
##################################################################

# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell proc_ip } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create ports
  set CLK100MHZ [ create_bd_port -dir I -type clk CLK100MHZ ]
  set GPIO [ create_bd_port -dir I -from 7 -to 0 GPIO ]
  set LED [ create_bd_port -dir O -from 15 -to 0 LED ]
  set RESETn [ create_bd_port -dir I -type rst RESETn ]
  set RS232_UART_RXD [ create_bd_port -dir O RS232_UART_RXD ]
  set RS232_UART_TXD [ create_bd_port -dir I RS232_UART_TXD ]

  # Create instance: axi_gpio_0, and set properties
  set axi_gpio_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_0 ]
  set_property -dict [ list \
   CONFIG.C_ALL_INPUTS {1} \
   CONFIG.C_ALL_OUTPUTS_2 {1} \
   CONFIG.C_GPIO2_WIDTH {16} \
   CONFIG.C_GPIO_WIDTH {16} \
   CONFIG.C_INTERRUPT_PRESENT {1} \
   CONFIG.C_IS_DUAL {1} \
 ] $axi_gpio_0

  # Create instance: axi_interconnect_0, and set properties
  set axi_interconnect_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_interconnect_0 ]
  set_property -dict [ list \
   CONFIG.ENABLE_ADVANCED_OPTIONS {0} \
   CONFIG.NUM_MI {6} \
   CONFIG.NUM_SI {1} \
   CONFIG.S00_HAS_DATA_FIFO {2} \
   CONFIG.S01_HAS_DATA_FIFO {2} \
   CONFIG.S02_HAS_DATA_FIFO {2} \
   CONFIG.S03_HAS_DATA_FIFO {2} \
   CONFIG.S04_HAS_DATA_FIFO {2} \
   CONFIG.S05_HAS_DATA_FIFO {2} \
   CONFIG.STRATEGY {2} \
 ] $axi_interconnect_0

  # Create instance: axi_uart16550_0, and set properties
  set axi_uart16550_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_uart16550:2.0 axi_uart16550_0 ]

  # Create instance: clk_wiz_0, and set properties
  set clk_wiz_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:6.0 clk_wiz_0 ]

  set_property -dict [ list \
   CONFIG.CLKOUT1_JITTER {118.758} \
   CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {166.667} \
   CONFIG.CLKOUT1_PHASE_ERROR {98.575}  \
   CONFIG.CLKOUT2_JITTER {114.829} \
   CONFIG.CLKOUT2_PHASE_ERROR {98.575} \
   CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {200} \
   CONFIG.CLKOUT2_USED {true} \
   CONFIG.CLKOUT3_JITTER {135.981} \
   CONFIG.CLKOUT3_PHASE_ERROR {98.575} \
   CONFIG.CLKOUT3_REQUESTED_OUT_FREQ {83.333} \
   CONFIG.CLKOUT3_USED {true} \
   CONFIG.CLK_IN1_BOARD_INTERFACE {sys_clock} \
   CONFIG.MMCM_DIVCLK_DIVIDE {1}  \
   CONFIG.MMCM_CLKOUT0_DIVIDE_F {6.000}  \
   CONFIG.MMCM_CLKOUT1_DIVIDE {5}  \
   CONFIG.MMCM_CLKFBOUT_MULT_F {10.000}  \
   CONFIG.MMCM_CLKOUT2_DIVIDE {12} \
   CONFIG.NUM_OUT_CLKS {3} \
   CONFIG.RESET_PORT {resetn} \
   CONFIG.RESET_TYPE {ACTIVE_LOW} \
   CONFIG.USE_BOARD_FLOW {true} \
 ] $clk_wiz_0

  # Create instance: proc_sys_reset_1, and set properties
  set proc_sys_reset_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 proc_sys_reset_1 ]
  set_property -dict [ list \
   CONFIG.C_AUX_RESET_HIGH {0} \
   CONFIG.C_AUX_RST_WIDTH {4} \
   CONFIG.C_EXT_RST_WIDTH {4} \
   CONFIG.C_NUM_BUS_RST {1} \
 ] $proc_sys_reset_1
puts "_______________GOT HERE 1_____________"

  # Create instance: bluespec_processor_0, and set properties
  set bluespec_processor_0 [ create_bd_cell -type ip -vlnv bluespec.com:ip:$proc_ip:E.1.0 bluespec_processor_0 ]
 
 puts "_______________GOT HERE 2_____________"

  # Create instance: xlconcat_1, and set properties
  # Concatenates GPIO
  set xlconcat_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_1 ]
  set_property -dict [ list \
   CONFIG.IN0_WIDTH {8} \
   CONFIG.NUM_PORTS {9} \
 ] $xlconcat_1

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
 ] $xlconstant_0

  # Create instance: xlconstant_1, and set properties
  set xlconstant_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_1 ]

  # Create instance: xilinx_jtag_0, and set properties
  set xilinx_jtag_0 [ create_bd_cell -type ip -vlnv bluespec.com:ip:xilinx_jtag:1.0 xilinx_jtag_0 ]

  # Create interface connections
  connect_bd_intf_net -intf_net axi_interconnect_0_M04_AXI [get_bd_intf_pins axi_interconnect_0/M00_AXI] [get_bd_intf_pins axi_uart16550_0/S_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_0_M05_AXI [get_bd_intf_pins axi_gpio_0/S_AXI] [get_bd_intf_pins axi_interconnect_0/M01_AXI]
  connect_bd_intf_net -intf_net bluespec_processor_0_dmem [get_bd_intf_pins axi_interconnect_0/S00_AXI] [get_bd_intf_pins bluespec_processor_0/master1]
  connect_bd_intf_net -intf_net jtag_connection [get_bd_intf_pins xilinx_jtag_0/jtag] [get_bd_intf_pins bluespec_processor_0/jtag]

  # Create port connections
  connect_bd_net -net axi_gpio_0_gpio2_io_o [get_bd_ports LED] [get_bd_pins axi_gpio_0/gpio2_io_o]

  # GPIO interrupt connection. Reenable after adding PLIC
  # connect_bd_net -net axi_gpio_0_ip2intc_irpt [get_bd_pins axi_gpio_0/ip2intc_irpt] [get_bd_pins xlconcat_0/In2]
  #
  # UART interrupt connection. Reenable after adding PLIC
  # connect_bd_net -net axi_uart16550_0_ip2intc_irpt [get_bd_pins axi_uart16550_0/ip2intc_irpt] [get_bd_pins xlconcat_0/In0]
  connect_bd_net -net axi_uart16550_0_sout [get_bd_ports RS232_UART_RXD] [get_bd_pins axi_uart16550_0/sout]

  # Connect up the clock
  connect_bd_net -net clk_in1_0_1 [get_bd_ports CLK100MHZ] [get_bd_pins clk_wiz_0/clk_in1]
  connect_bd_net -net clk_network [get_bd_pins clk_wiz_0/clk_out3] [get_bd_pins axi_gpio_0/s_axi_aclk] [get_bd_pins axi_interconnect_0/ACLK] [get_bd_pins axi_interconnect_0/M00_ACLK] [get_bd_pins axi_interconnect_0/M01_ACLK] [get_bd_pins axi_interconnect_0/M02_ACLK] [get_bd_pins axi_interconnect_0/M03_ACLK] [get_bd_pins axi_interconnect_0/M04_ACLK] [get_bd_pins axi_interconnect_0/M05_ACLK] [get_bd_pins axi_interconnect_0/S00_ACLK] [get_bd_pins axi_uart16550_0/s_axi_aclk] [get_bd_pins proc_sys_reset_1/slowest_sync_clk] [get_bd_pins bluespec_processor_0/CLK] [get_bd_pins xilinx_jtag_0/clk]

  # Connect up the reset
  connect_bd_net -net ext_reset_in [get_bd_ports RESETn] [get_bd_pins clk_wiz_0/resetn] [get_bd_pins proc_sys_reset_1/aux_reset_in]
  connect_bd_net -net tiehigh [get_bd_pins xlconstant_1/dout] [get_bd_pins proc_sys_reset_1/ext_reset_in] [get_bd_pins proc_sys_reset_1/dcm_locked]
  
  connect_bd_net -net reset_network [get_bd_pins proc_sys_reset_1/peripheral_aresetn] [get_bd_pins axi_gpio_0/s_axi_aresetn] [get_bd_pins bluespec_processor_0/RST_N] [get_bd_pins axi_uart16550_0/s_axi_aresetn] [get_bd_pins xilinx_jtag_0/rst_n] [get_bd_pins axi_interconnect_0/ARESETN] [get_bd_pins axi_interconnect_0/M00_ARESETN] [get_bd_pins axi_interconnect_0/M01_ARESETN] [get_bd_pins axi_interconnect_0/M02_ARESETN] [get_bd_pins axi_interconnect_0/M03_ARESETN] [get_bd_pins axi_interconnect_0/M04_ARESETN] [get_bd_pins axi_interconnect_0/M05_ARESETN] [get_bd_pins axi_interconnect_0/S00_ARESETN]

  connect_bd_net -net dmi_reset_in [get_bd_pins xilinx_jtag_0/reset] [get_bd_pins bluespec_processor_0/TRST]

  # Concat the GPIOs
  connect_bd_net -net gpio_1 [get_bd_ports GPIO] [get_bd_pins xlconcat_1/In0]

  # GPIO to pads
  connect_bd_net -net xlconcat_1_dout [get_bd_pins axi_gpio_0/gpio_io_i] [get_bd_pins xlconcat_1/dout]

  # UART to pads
  connect_bd_net -net rs232_uart_rxd_1 [get_bd_ports RS232_UART_TXD] [get_bd_pins axi_uart16550_0/sin]

  # Reintroduce with PLIC. This will be interrupt input to the PLIC
  # connect_bd_net -net xlconcat_0_dout [get_bd_pins bluespec_processor_0/cpu_external_interrupt_req] [get_bd_pins xlconcat_0/dout]
  #
  connect_bd_net -net xlconstant_0_dout [get_bd_pins axi_uart16550_0/freeze] [get_bd_pins proc_sys_reset_1/mb_debug_sys_rst] [get_bd_pins xlconcat_1/In1] [get_bd_pins xlconcat_1/In2] [get_bd_pins xlconcat_1/In3] [get_bd_pins xlconcat_1/In4] [get_bd_pins xlconcat_1/In5] [get_bd_pins xlconcat_1/In6] [get_bd_pins xlconcat_1/In7] [get_bd_pins xlconcat_1/In8] [get_bd_pins xlconstant_0/dout]

  # Tie off CPU interrupt pins. The interrupt pins will be reconnected after
  # introducing the PLIC and CLINT.
  connect_bd_net [get_bd_pins bluespec_processor_0/ext_interrupt] [get_bd_pins xlconstant_0/dout]
  connect_bd_net [get_bd_pins bluespec_processor_0/sw_interrupt] [get_bd_pins xlconstant_0/dout]
  connect_bd_net [get_bd_pins bluespec_processor_0/timer_interrupt] [get_bd_pins xlconstant_0/dout]

  # Create address segments
  create_bd_addr_seg -range 0x00010000 -offset 0x6FFF0000 [get_bd_addr_spaces bluespec_processor_0/master1] [get_bd_addr_segs axi_gpio_0/S_AXI/Reg] SEG_axi_gpio_0_Reg
  create_bd_addr_seg -range 0x00001000 -offset 0x62300000 [get_bd_addr_spaces bluespec_processor_0/master1] [get_bd_addr_segs axi_uart16550_0/S_AXI/Reg] SEG_axi_uart16550_0_Reg

  # Restore current instance
  current_bd_instance $oldCurInst

  validate_bd_design
  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design "" $proc_ip
