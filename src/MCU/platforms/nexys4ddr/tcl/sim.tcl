add_force {/design_1_wrapper/RESETn} -radix hex {0 0ns} {1 500ns}
add_force {/design_1_wrapper/RS232_UART_TXD} -radix hex {1 0ns}
add_force {/design_1_wrapper/CLK100MHZ} -radix hex {1 0ns} {0 5ns} -repeat_every 10ns
# add_force {/design_1/bluespec_processor_0/inst/dmi_reset1/RESET_OUT} -radix hex {0 0ns} {1 40ns} -- no such signal for Rocket designs
open_vcd xsim_dump.vcd
log_vcd /
run 600ns
