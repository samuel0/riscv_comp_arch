# -*- gdb-script -*-

set architecture riscv:rv32
set remotetimeout 5000
set remotelogfile /tmp/gdb-remote.log
set logging overwrite
set logging file /tmp/gdb-client.log
set logging on
set pagination off

define reset_and_halt
  dont-repeat
  monitor riscv dmi_write 0x10 0x80000003
  monitor riscv dmi_write 0x10 0x80000001
  printf "Reseting CPI\n"
end

target remote | openocd --file openocd.cfg --log_output /tmp/openocd.log --debug
