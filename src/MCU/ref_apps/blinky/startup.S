	.text
	.globl	_reset

_reset:
	// set stack pointer
	la	t0, _reset
	li	t1, 0x1fff0
	add	sp, t0, t1
	// Place an illegal instruction (0) at the stack pointer
	// so that the program ends if the value is used
	sw	zero, 0(sp)
	sw	zero, -4(sp)

	// Set MTVEC to an address that just sends all 1s to the GPIOs
        // followed by a self-loop
	la	s0, trap
	csrw	mtvec, s0

	j _start

        // trap handler. Make all RGB LEDs glow red as a visual indication that we
        // have trapped, and then stay in a self loop
trap:   la      s0, 0x6fff0000
        li      s1, 0x9240
        sb      s1, 8(s0)
stay:   j       stay
