// This file holds memory mapped IO addresses
// 
// --------
// The following definition points to the address of the GPIO2_DATA register of
// the AXI GPIO IP
// (https://www.xilinx.com/support/documentation/ip_documentation/axi_gpio/v2_0/pg144-axi-gpio.pdf)
// page 10.
// The base address matches the location of the IP in soc_bd.tcl
#define GPIO2_DATA (0x6fff0008ULL)
 
// --------
