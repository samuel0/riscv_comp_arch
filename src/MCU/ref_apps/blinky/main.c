#include <stdio.h>

// for read_cycle
#include "riscv_counters.h"

// for PRIu64
#include <inttypes.h>

// For memory mapped IO
#include "soc_map.h"

int main (int argc, char ** argv)
{
   volatile unsigned char * gpio_addr = (void*)GPIO2_DATA;
   unsigned char pattern = 0;
   unsigned char iter = 0;
   unsigned bitpos = 0;
   unsigned int delay = 0;
   uint64_t start=read_cycle();

   *gpio_addr = 0x10;   // indicate SoT   // RGB-LED glows blue

   // A delay loop
   for (delay = 0; delay < (512*1024); delay++)
      __asm__ __volatile__ ("" ::: "memory");

   printf ("Starting LED sequence ... \n");

   // The LED loop
   for (iter=1; iter < 16; iter++) {
     // increment the gpio val
     (*gpio_addr)++;

     // Print the LED pattern
     printf("LED: ");

     for (bitpos=0; bitpos<4; bitpos++) {
        pattern = (iter >> bitpos) & 0x01;
        if (pattern == 0) printf (" - ");
        else              printf (" X ");
     }
     printf("\n");

     // A delay loop -- the "memory" in the clobber field ensures
     // that gcc optimization flags do not optimize the loop away.
     for (delay = 0; delay < (1024*1024); delay++)
        __asm__ __volatile__ ("" ::: "memory");
   }

   printf ("LED Sequence Complete. \n");
   uint64_t end=read_cycle();
   printf("read_cycle: %" PRIu64 " cycles have elapsed.\n",end-start);

   *gpio_addr = 0x20;   // indicate successful EoT // RGB-LED glows green
}
